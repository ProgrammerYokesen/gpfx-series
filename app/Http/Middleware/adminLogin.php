<?php

namespace App\Http\Middleware;

use Closure;
use CRUDBooster;

class adminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(CRUDBooster::myId()){
            return $next($request);
        }
        return redirect('https://gpfxseries.com/competitionadmin/login');
    }
}
