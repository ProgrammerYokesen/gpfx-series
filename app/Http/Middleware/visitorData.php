<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Input;

class visitorData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Input::get('utm_campaign')){
          Cookie::queue('utm_campaign', Input::get('utm_campaign'), 3243200);
        }

        if(Input::get('utm_medium')){
          Cookie::queue('utm_medium', Input::get('utm_medium'), 3243200);
        }

        if(Input::get('utm_source')){
          Cookie::queue('utm_source', Input::get('utm_source'), 3243200);
        }

        if(Input::get('utm_content')){
          Cookie::queue('utm_content', Input::get('utm_content'), 3243200);
        }

        if(Input::get('ref')){
          Cookie::queue('ref', Input::get('ref'), 3243200);
        }

        return $next($request);
    }
}
