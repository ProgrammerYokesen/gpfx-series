<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Logon
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Session::get('user.id_cms_privileges')=='2'){

        $client = new Client();

        $client = new Client();
        $profile_get = $client->get(env('API_BASE_URL').'profile_detail?user_id='.Session::get('user.id'), [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ]
        ]);

        $profile = json_decode($profile_get->getBody());
        Session::put('user.profile.status',$profile->status);
        $today = time();
        $datediff = $today - strtotime($profile->created_at);
        $days = round($datediff / (60 * 60 * 24));
        Session::put('user.profile.created_at',$days);

        $client = new Client();
        $account_list = $client->get(env('API_BASE_URL').'account_listing?user_id='.Session::get('user.id').'&status=approved', [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ]
        ]);

        $accounts = json_decode($account_list->getBody());
        Session::put('user.mt4.num',count($accounts->data));

        $deposit_listing = $client->get(env('API_BASE_URL').'deposit_listing?user_id='.Session::get('user.id'), [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ]
        ]);

        $depo = json_decode($deposit_listing->getBody());
        $depos = $depo->data;
        $xdp = 0;
        foreach ($depos as $dpsit) {
          if ($dpsit->status == 'approved') {
            $xdp++;
          }
        }

        Session::put('user.deposit',$xdp);

        return $next($request);

      }else{
        return redirect('/');
      }
    }
}
