<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Session;
use Carbon\Carbon;

class klasemenController extends Controller
{
    public function klasemenPage()
    {
        $user_id = Session::get('user.id');
        $klasemen = DB::table('users')
            ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
            ->join('signals', 'signals.login', 'trading_account.loginId')
            ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
            ->groupBy('users.id')
            ->where('trading_account.type', 'GPFX Series')
            ->limit(10);
        $klasemenTotal["klasemen"] = $klasemen->get()->sortByDesc('profit')->values();
        $checkTopTen = $klasemen->where('users.id', $user_id)->count();
        $checkTopTen = 0;
        if ($checkTopTen > 0) {
            $checkTopTen = 1;
        }
        $klasemenTotal['topTen'] = $checkTopTen;
        $klasemenTotal["selfData"] = $klasemen->where('users.id', $user_id)->first();

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $klasemen = DB::table('users')
                    ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
                    ->join('signals', 'signals.login', 'trading_account.loginId')
                    ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
                    ->groupBy('users.id')
                    ->where('trading_account.type', 'GPFX Series')
                    ->limit(10)
                    ->whereBetween('trading_account.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        $klasemenMingguan["klasemen"] = $klasemen->get()->sortByDesc('profit')->values();
        $checkTopTen = $klasemenMingguan["klasemen"]->where('users.id', $user_id)->count();
        $topTen = 0;
        if ($topTen > 0) {
            $topTen = 1;
        }
        $klasemenMingguan['topTen'] = $topTen;
        $klasemenMingguan["selfData"] = $klasemen->where('users.id', $user_id)->first();
        return view('pages.klasemen', compact('klasemenTotal', 'klasemenMingguan'));
    }

    public function announcementPage()
    {
        $data['announcements'] = DB::table('announcements')
            ->orderBy('created_at', 'asc')
            ->get();
        // dd($data);
        return view('pages.announcement', $data);
    }

}
