<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Cookie;

class DashboardController extends Controller
{
  public function profile(){
    $user_id = Session::get('user.id');
    $client = new Client();
    $profile_get = $client->get(env('API_BASE_URL').'profile_detail?user_id='.$user_id, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $profile = json_decode($profile_get->getBody());

    if($profile->api_message== "There is no data found !"){
      $client = new Client();
      $profile_create = $client->post(env('API_BASE_URL').'create_profile', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'fullname' => Session::get('user.name'),
          'email' => Session::get('user.email'),
          'parent' => Session::get('user.parent'),
          'status' => 'active',
          'user_id' =>  Session::get('user.id')
        ]
      ]);

      $profile = json_decode($profile_create->getBody());
      Alert::success('Creating Profile','Please Wait...');
      return redirect()->route('dashboard');
    }

    $client = new Client();
    $bank_get = $client->get(env('API_BASE_URL').'bank_detail?user_id='.$user_id, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $bank = json_decode($bank_get->getBody());

    if($bank->api_message== "There is no data found !"){
      $client = new Client();
      $bank_create = $client->post(env('API_BASE_URL').'create_bank', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'account_name' => Session::get('user.name'),
          'user_id' => Session::get('user.id')
        ]
      ]);
      $bank = json_decode($bank_create->getBody());
      Alert::success('Creating Bank Data','Please Wait...');
      return redirect()->route('dashboard');
    }
    return view('pages.dashboard',compact('profile','bank'));
  }
}
