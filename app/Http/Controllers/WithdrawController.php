<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Cookie;

class WithdrawController extends Controller
{
    function withdrawList(){
      $user_id = Session::get('user.id');

      $client = new Client();


      $account_list = $client->get(env('API_BASE_URL').'account_listing?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $accounts = json_decode($account_list->getBody());
      $mt4s = $accounts->data;

      $bank_get = $client->get(env('API_BASE_URL').'bank_detail?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $bank = json_decode($bank_get->getBody());

      $client = new Client();
      $deposit_pending = $client->get(env('API_BASE_URL').'withdrawal_history?user_id='.$user_id.'&status=pending', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $pending = json_decode($deposit_pending->getBody());
      //dd($pending);
      $pendings = $pending->data;

      $client = new Client();
      $withdrawal_history = $client->get(env('API_BASE_URL').'withdrawal_history?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $history = json_decode($withdrawal_history->getBody());
      //dd($pending);
      $histories = $history->data;


      return view('pages.withdraw',compact('mt4s','bank','pendings','histories'));
    }

    function withdrawRequest(Request $request){
      $user_id = Session::get('user.id');
      $user_name = Session::get('user.name');

      $client = new Client();
      $transfer_create = $client->post(env('API_BASE_URL').'withdrawal_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'metatrader' => $request->metatrader,
          'user_id' => $user_id,
          'amount' => $request->amount,
          'status' => 'pending',
          'signatures' => strtotime(date('Y-m-d H:i:s')),
          'bank_name' => $request->bankname,
          'account_name' => $request->bankaccount,
          'account_number' => $request->banknumber,
        ]
      ]);

      /**paket email**/
      /*
      $time_email = date('d-m-Y H:i');
      $name_email = Session::get('user.name');
      $email_email = Session::get('user.email');
      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('mail.withdrawal_request', [
        'name' => $name_email,
        'email' => $email_email,
        'time' => $time_email,
        'tomt4id' => $request->metatrader,
        'amount' => $request->amount,
        'fromBank' => $request->bankname,
        'fromName' => $request->bankaccount,
        'fromNumber' => $request->banknumber
      ],
        function($message) use ($name_email, $email_email)
      {
          $message
            ->from('noreply@FinanciaFx.com','FinanciaFx Service')
              ->to($email_email,$name_email)
                ->subject('Withdrawal Request FinanciaFx');
      });
      */

      $client = new Client();
      $log = $client->post(env('API_BASE_URL').'log_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'user_id' => $user_id,
          'area' => 'user',
          'kegiatan' => 'withdrawal',
          'keterangan' => 'Pengajuan withdrawal dikirim',
        ]
      ]);

      /**sampai sini**/
      Alert::success('Pengajuan Withdrawal akan segera kami prosess','Berhasil ');
      return redirect()->route('listWithdraw');
    }
}
