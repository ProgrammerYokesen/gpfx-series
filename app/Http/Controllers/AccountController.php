<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Cookie;

class AccountController extends Controller
{
    public function listAccount(){
      $user_id = Session::get('user.id');

      $client = new Client();
      $account_list = $client->get(env('API_BASE_URL').'account_listing?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $accounts = json_decode($account_list->getBody());
      $lists = $accounts->data;

      $client = new Client();
      $deposit_list = $client->get(env('API_BASE_URL').'deposit_listing?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $deposits = json_decode($deposit_list->getBody());
      $depolists = $deposits->data;

      return view('pages.account',compact('lists','depolists'));

    }

    public function requestAccount(Request $request){
      $time = time();
      $client = new Client();
      $id = '13';
      switch($id){
        case '1' : $type = 'Bronze Account';break;
        case '2' : $type = 'Silver Account';break;
        case '3' : $type = 'Gold Account';break;
        case '4' : $type = 'Basic Account';break;
        case '5' : $type = 'VIP Account';break;
        case '6' : $type = 'VVIP Account';break;
        case '7' : $type = 'Index Asia Mini';break;
        case '8' : $type = 'Index Asia Reguler';break;
        case '9' : $type = 'Syariah';break;
        case '10' : $type = 'ContestJan2020';break;
        case '11' : $type = 'VVIP Account';break;
        case '12' : $type = 'Syariah';break;
        case '13' : $type = 'GPFX Series';break;
      }

      $client = new Client();
      $account_list = $client->get(env('API_BASE_URL').'account_max?type='.$type, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $accounts = json_decode($account_list->getBody());
      $lists = $accounts->data;
      $max = $lists[0]->login;
      $new = $max + 1;

      if($id == '11' || $id == '6'){

        $client = new Client();
        $account_list_promo = $client->get(env('API_BASE_URL').'account_max?type=Promo2020', [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ]
        ]);

        $accounts_promo = json_decode($account_list_promo->getBody());
        $lists_promo = $accounts_promo->data;
        $max_promo = $lists_promo[0]->login;
        if($max_promo > $max){
          $new = $max_promo + 1;
        }

      }

      if($id == '12' || $id == '9'){

        $client = new Client();
        $account_list_promo = $client->get(env('API_BASE_URL').'account_max?type=ContestNov2020', [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ]
        ]);

        $accounts_promo = json_decode($account_list_promo->getBody());
        $lists_promo = $accounts_promo->data;
        $max_promo = $lists_promo[0]->login;
        if($max_promo > $max){
          $new = $max_promo + 1;
        }

      }



      $user_id = Session::get('user.id');

      $account_terakhir = $client->get(env('API_BASE_URL').'account_terakhir?user_id='.$user_id.'&type='.$type.'&status=waiting', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);
      $terakhir = json_decode($account_terakhir->getBody());

      //dd($lists,$lists_promo,"id : ".$id,"max:".$max,"max_promo:".$max_promo,"new:".$new,"type:".$type,$terakhir);

      if($id == '11'){
        $type = "Promo2020";
      }

      if($id == '12'){
        $type = "ContestNov2020";
      }


            if($terakhir->api_message == 'There is no data found !'){
              $account_create = $client->post(env('API_BASE_URL').'account_create', [
                'headers' => [
                  'X-Authorization-Token' => md5(env('API_KEY')),
                  'X-Authorization-Time'  => time()
                ],
                'form_params' => [
                  'type' => $type,
                  'username' => Session::get('user.name'),
                  'user_id' =>  Session::get('user.id'),
                  'status' => 'waiting',
                  'login' => $new
                ]
              ]);

              $accounts = json_decode($account_create->getBody());
              if($accounts->api_message=='success'){
                $client = new Client();
                $log = $client->post(env('API_BASE_URL').'log_create', [
                  'headers' => [
                    'X-Authorization-Token' => md5(env('API_KEY')),
                    'X-Authorization-Time'  => time()
                  ],
                  'form_params' => [
                    'user_id' => $user_id,
                    'area' => 'account',
                    'kegiatan' => 'request account',
                    'keterangan' => 'Mengajukan permintaan akun MT4',
                  ]
                ]);
            }

      }else{
        Alert::error('Mohon Maaf, silahkan menunggu pembuatan akun yang terakhir.','Mohon ditunggu')->persistent('Close');
        return redirect()->route('listAccount');
      }

      Alert::success('Permintaan pembuatan akun MT4 telah kami terima, silahkan cek notifikasi secara berkala.','Mohon ditunggu');
      return redirect()->route('listAccount');
    }

    public function resetAccount(Request $request,$id){
      $client = new Client();

      $mt4_detail = $client->get(env('API_BASE_URL').'metatrader_detail?id='.$id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $detail_meta = json_decode($mt4_detail->getBody());

      $profile_detail = $client->get(env('API_BASE_URL').'profile_detail?user_id='.$detail_meta->user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $trader_detail = json_decode($profile_detail->getBody());

      /**paket email**/
      /*
      $time_email = date('d-m-Y H:i');
      $name_email = $trader_detail->fullname;
      $email_email = $trader_detail->email;
      $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
      $beautymail->send('mail.reset_mt4_password', [
        'name' => $name_email,
        'email' => $email_email,
        'time' => $time_email,
        'mt4id' => $detail_meta->login
      ],
        function($message) use ($name_email, $email_email)
      {
          $message
            ->from('noreply@FinanciaFx.com','FinanciaFx Service')
              ->to($email_email,$name_email)
                ->subject('Reset Password MT4 Financia Fx');
      });
      */
      /**sampai sini**/

      $metatrader_submit = $client->post(env('API_BASE_URL').'metatrader_update', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'id' => $id,
          'status' => 'reset'
        ]
      ]);

      /*log reset disini*/
      $log = $client->post(env('API_BASE_URL').'log_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'user_id' => $trader_detail->user_id,
          'area' => 'user',
          'kegiatan' => 'request reset mt4 password',
          'keterangan' => 'Mengajukan permintaan reset mt4 password '.$detail_meta->login,
        ]
      ]);

      Alert::success('Permintaan reset password berhasil','Mohon ditunggu');
      return redirect()->back();
    }
}
