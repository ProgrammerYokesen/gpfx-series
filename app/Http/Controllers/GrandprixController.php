<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Image;
use Cookie;
use Storage;

class GrandprixController extends Controller
{
  public function pendaftaran(){
    $user_id = Session::get('user.id');
    $client = new Client();
    $profile_get = $client->get(env('API_BASE_URL').'profile_detail?user_id='.$user_id, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $profile = json_decode($profile_get->getBody());
    // dd($profile);
    if($profile->api_message== "There is no data found !"){
      $client = new Client();
      $profile_create = $client->post(env('API_BASE_URL').'create_profile', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'fullname' => Session::get('user.name'),
          'email' => Session::get('user.email'),
          'parent' => Session::get('user.parent'),
          'status' => 'active',
          'user_id' =>  Session::get('user.id')
        ]
      ]);

      $profile = json_decode($profile_create->getBody());
      Alert::success('Creating Profile','Please Wait...');
      return redirect()->route('dashboard');
    }

    $client = new Client();
    $bank_get = $client->get(env('API_BASE_URL').'bank_detail?user_id='.$user_id, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $bank = json_decode($bank_get->getBody());

    if($bank->api_message== "There is no data found !"){
      $client = new Client();
      $bank_create = $client->post(env('API_BASE_URL').'create_bank', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'account_name' => Session::get('user.name'),
          'user_id' => Session::get('user.id')
        ]
      ]);
      $bank = json_decode($bank_create->getBody());
      Alert::success('Creating Bank Data','Please Wait...');
      return redirect()->route('dashboard');
    }
    // dd($profile);
    return view('pages.pendaftaran-gpfx',compact('profile','bank'));
  }

  public function update(Request $request){
      $validator = \Validator::make($request->all(), [
            'fullname' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'bankname' => 'required',
            'banknumber' => 'required',
            'bankaccount' => 'required',
            'uploadid' => 'required',
            'uploadaddress' => 'required',
            'daftar-traderindo' => 'required',
        ]);
        if($validator->fails()){
            return response()->json(['failed',$validator->errors()]);
        }

    try{
    $user_id = Session::get('user.id');
    $client = new Client();
    $profile_edit = $client->post(env('API_BASE_URL').'profile_edit', [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ],
      'form_params' => [
        'id' => $request->id,
        'fullname' => $request->fullname,
        'user_id' => $user_id,
        'phone' => $request->phone,
        'dob' => $request->dob
      ]
    ]);

    $profile = json_decode($profile_edit->getBody());

    if($profile->api_message=='success'){
      $client = new Client();

      $get_check_all = $client->get(env('API_BASE_URL').'check_get_all?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $gca = json_decode($get_check_all->getBody());

      if($gca->check_profile == 0){
        $check_all = $gca->check_all + 1;
        $pid = $gca->id;

        $check_bank = $client->post(env('API_BASE_URL').'check_all?id='.$pid, [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ],
          'form_params' => [
            'id' => $pid,
            'check_profile' => '1',
            'check_all' => $check_all
          ]
        ]);
      }

      $log = $client->post(env('API_BASE_URL').'log_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'user_id' => $user_id,
          'area' => 'profile',
          'kegiatan' => 'Update Profile',
          'keterangan' => 'Berhasil memperbaharui detil bank',
        ]
      ]);
    }

    /*start from here */
    $user_id = Session::get('user.id');
    $client = new Client();
    $profile_get = $client->get(env('API_BASE_URL').'profile_detail?user_id='.$user_id, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $profilex = json_decode($profile_get->getBody());

    if($request->file('uploadid'))
      {
        $image = $request->file('uploadid');
        $time = time();
        $img1  = str_replace(" ","",$time."-".$image->getClientOriginalName());
        $image_normal = Image::make($image)->widen(600, function ($constraint)
        {
          $constraint->upsize();
        });

        $image_thumb = Image::make($image)->fit(200, 200);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        Storage::disk('local')->put('public/'.$img1, $image_normal->__toString());
        Storage::disk('local')->put('public/thumbnails/'.$img1, $image_thumb->__toString());
      }
      else
      {
        $img1 = $profilex->uploadid;
      }

      if($request->file('uploadaddress'))
        {
          $image = $request->file('uploadaddress');
          $time = time();
          $img2  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('public/'.$img2, $image_normal->__toString());
          Storage::disk('local')->put('public/thumbnails/'.$img2, $image_thumb->__toString());
        }
        else
        {
          $img2 = $profilex->uploadaddress;
        }


    $client = new Client();
    $profile_edit = $client->post(env('API_BASE_URL').'profile_edit', [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ],
      'form_params' => [
        'id' => $request->id,
        'user_id' => $user_id,
        'uploadid' => $img1,
        'uploadaddress' => $img2,
      ]
    ]);

    $profile = json_decode($profile_edit->getBody());

    if($profile->api_message=='success'){
      $client = new Client();

      $get_check_all = $client->get(env('API_BASE_URL').'check_get_all?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $gca = json_decode($get_check_all->getBody());

      if($gca->check_photo == 0){
        $check_all = $gca->check_all + 1;
        $pid = $gca->id;

        $check_bank = $client->post(env('API_BASE_URL').'check_all?id='.$pid, [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ],
          'form_params' => [
            'id' => $pid,
            'check_photo' => '1',
            'check_all' => $check_all
          ]
        ]);
      }

      $log = $client->post(env('API_BASE_URL').'log_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'user_id' => $user_id,
          'area' => 'profile',
          'kegiatan' => 'Update Profile',
          'keterangan' => 'Berhasil upload/memperbarui foto identitas',
        ]
      ]);
    }
    /*end here*/

    /*iniyang bank*/
    $user_id = Session::get('user.id');
    $client = new Client();
    $bank_edit = $client->post(env('API_BASE_URL').'bank_edit', [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ],
      'form_params' => [
        'id' => $request->bank_id,
        'user_id' => $user_id,
        'bank_name' => $request->bankname,
        'account_name' => $request->bankaccount,
        'account_number' => $request->banknumber
      ]
    ]);

    $bank = json_decode($bank_edit->getBody());

    if($bank->api_message=='success'){
      $client = new Client();

      $get_check_all = $client->get(env('API_BASE_URL').'check_get_all?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $gca = json_decode($get_check_all->getBody());

      if($gca->check_bank == 0){
        $check_all = $gca->check_all + 1;
        $pid = $gca->id;

        $check_bank = $client->post(env('API_BASE_URL').'check_all?id='.$pid, [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ],
          'form_params' => [
            'id' => $pid,
            'check_bank' => '1',
            'check_all' => $check_all
          ]
        ]);
      }

      $log = $client->post(env('API_BASE_URL').'log_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'user_id' => $user_id,
          'area' => 'profile',
          'kegiatan' => 'Update Profile',
          'keterangan' => 'Berhasil memperbaharui detil bank',
        ]
      ]);
    }
    /*selesai ganti bank*/

    /*Traderindo*/
    
      $register_traderindo = $client->post(env('API_TRADERINDO_END_POINT').'register', [
      'form_params' => [
        'name' => $request->fullname,
        'email' => Session::get('user.email'),
        'password' => 'autopasswordfromgpfx',
        'password_confirmation' => 'autopasswordfromgpfx',
        'ref_id' => 0,
        'utm_campaign' => 'Grandprix Forex 2021',
        'utm_source' => 'web gpfx',
        'utm_medium' => 'pendaftaran gpfx',
        'utm_content' => Cookie::get('utm_content')
       ]
     ]); 
        $responseRegister = json_decode($register_traderindo->getBody());
        return response()->json($responseRegister);
    
    }catch(\GuzzleHttp\Exception\RequestException $e){
         $responseRegister = 'failed';
        return response()->json($responseRegister);
    }
    /*traderindo*/
    
  }

}
