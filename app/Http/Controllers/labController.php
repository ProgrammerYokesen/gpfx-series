<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Session;
use Carbon\Carbon;

class labController extends Controller
{
    public function registerData()
    {
        $client = new Client();
        $result = $client->get('http://fed.financiafx.com/api/gpfxseries_register');
        $res = json_decode($result->getBody());
        dd($res);
    }

    public function verify(){
        return view('lab.verify-code');
    }

    public function registerUser(Request $request)
    {

        $rules = array(
            'name' => 'required|min:3|max:60',
            'email' => 'required',
            'wa' => 'required',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        );

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->route('daftar')->withInput()->withErrors($validator);
        }

        $verificationCode = Str::random(8);

        \DB::table('registerData')->insert([
            'name' => $request->name,
            'email' => $request->email,
            'phoneNumber' => $request->wa,
            'password' =>  $request->password,
            'ref' => $request->ref,
            'campaign' => $request->campaign,
            'so' => $request->so,
            'uuid' => $request->uuid,
            'disclaimer' => $request->disclaimer,
            'dari' => $request->dari,
            'verificationCode' => $verificationCode,
            'verify' => 0
        ]);

        return view('lab.verify-code');
    }

    public function listing($id)
    {
        //dd($id);
        $pull = DB::table('whatsapp_msg')->where('id', $id)->first();
        $group = $pull->grup;
        $members = DB::table('subscribers')->where('group_2', $group)->where('status', 'active')->get();
        foreach ($members as $n => $member) {
            $nama = explode(" ", $member->nama);
            $messages = "$pull->caption dikirim untuk $member->nama $member->whatsapp";
            $insert = DB::table('daftar_kiriman')->insert([
                'phone' => $member->whatsapp,
                'content' => $messages,
                'nama' => $member->nama,
                'group_1' => $member->group_1,
                'group_2' => $member->group_2,
                'msg_id' => $id
            ]);
        }
        if ($group != '1') {
            $members = DB::table('subscribers')->where('group_2', '1')->where('status', 'active')->get();
            foreach ($members as $n => $member) {
                $nama = explode(" ", $member->nama);
                $messages = "Halo Sdr/i $member->nama, $pull->caption";
                $insert = DB::table('daftar_kiriman')->insert([
                    'phone' => $member->whatsapp,
                    'content' => $messages,
                    'nama' => $member->nama,
                    'group_1' => $member->group_1,
                    'group_2' => $member->group_2,
                    'msg_id' => $id
                ]);
            }
        }


        $update = DB::table('whatsapp_msg')->where('id', $pull->id)->update([
            'flag' => '1'
        ]);
        return redirect()->back();
    }

    public function kirim(){
        date_default_timezone_set("Asia/Jakarta");
        $date = date('Y-m-d');
        $number_of_subscribers = DB::table('daftar_kiriman')->where('group_2','!=','1')->whereDate('send_at',$date)->get();
        $number_of_messages = DB::table('daftar_kiriman')->where('group_2','!=','1')->whereDate('send_at',$date)->select('phone')->distinct()->get();
  
        $nus = count($number_of_subscribers);
        $num = count($number_of_messages);
  
        if($nus <2000 && $num < 2000){
          echo "number of subsrciber ".$nus."<br>";
          echo "number of message ".$num."<br>";
  
          $client = new Client();
          $pull = DB::table('daftar_kiriman')->where('flag','2')->limit('10')->get();
          foreach($pull as $n => $isi){
            $check_queue = $client->post(env('CHAT-API-PATH').'showMessagesQueue?token='.env('CHAT-API-TOKEN'));
            $decode_queue = json_decode($check_queue->getBody());
            $queue = $decode_queue->totalMessages;
            echo $queue."<br>";
  
            if($queue < 8){
              $res = $client->post(env('CHAT-API-PATH').'sendMessage?token='.env('CHAT-API-TOKEN'),[
                'form_params' => [
                  "phone" => $isi->phone,
                  "body" => $isi->content
                ]
              ]);
              $date = date('Y-m-d H:i:s');
  
              $update = DB::table('daftar_kiriman')->where('id',$isi->id)->update([
                'flag' => '1',
                'send_at' => $date
              ]);
            }
            sleep(1);
          }
        }
      }
      
      public function tradingAccount(Request $request){
                $client = new Client();
                  $account_list = $client->get(env('API_BASE_URL').'account_listing?user_id='.$request->user_id, [
                    'headers' => [
                      'X-Authorization-Token' => md5(env('API_KEY')),
                      'X-Authorization-Time'  => time()
                    ]
                  ]);
            
                  $accounts = json_decode($account_list->getBody());
                  $lists = $accounts->data;
                  dd($lists);
      }

    public function metaTraderData(){
        $client = new Client();
        $mt4_detail = $client->get(env('API_BASE_URL') . 'metatrader_login?login=888660019');
        $res = json_decode($mt4_detail->getBody());
        dd($res);
    }

    public function announcementPage()
    {
        $data['announcements'] = DB::table('announcements')
            ->orderBy('created_at', 'asc')
            ->get();
        // dd($data);
        return view('labs.announcement', $data);
    }

    public function klasemenPage()
    {
        $user_id = Session::get('user.id');
        $klasemen = DB::table('users')
            ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
            ->join('signals', 'signals.login', 'trading_account.loginId')
            ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
            ->groupBy('users.id')
            ->where('trading_account.type', 'GPFX Series')
            ->limit(10);
        $klasemenTotal["klasemen"] = $klasemen->get();
        $checkTopTen = $klasemen->where('users.id', $user_id)->count();
        $checkTopTen = 0;
        if ($checkTopTen > 0) {
            $checkTopTen = 1;
        }
        $klasemenTotal['topTen'] = $checkTopTen;
        $klasemenTotal["selfData"] = $klasemen->where('users.id', $user_id)->first();

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $klasemen = DB::table('users')
                    ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
                    ->join('signals', 'signals.login', 'trading_account.loginId')
                    ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
                    ->groupBy('users.id')
                    ->where('trading_account.type', 'GPFX Series')
                    ->limit(10)
                    ->whereBetween('trading_account.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        $klasemenMingguan["klasemen"] = $klasemen->get();
        $checkTopTen = $klasemenMingguan["klasemen"]->where('users.id', $user_id)->count();
        $topTen = 0;
        if ($topTen > 0) {
            $topTen = 1;
        }
        $klasemenMingguan['topTen'] = $topTen;
        $klasemenMingguan["selfData"] = $klasemen->where('users.id', $user_id)->first();
        return view('labs.klasemen', compact('klasemenTotal', 'klasemenMingguan'));
    }
}
