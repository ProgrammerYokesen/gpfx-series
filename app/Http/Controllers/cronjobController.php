<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class cronjobController extends Controller
{
    public function sendEmail(){
        $data = DB::table('email_queues')->join('email_templates', 'email_templates.id', 'email_queues.email_template_id')
            ->join('users', 'users.email','email_queues.email_recipient')
            ->where('is_sent', 0)
            ->orWhere(function($query){
                $query->where('email_queues.type', '!=', 'onetime')->where('is_stop', 0);
            })
            ->select('email_queues.id as id', 'email_queues.email_recipient as email','email_templates.subject as subject', 'users.name as name', 'email_queues.type as type','email_templates.name as templateName', 'users.uuid as uuid', 'email_queues.send_at as send_at', 'email_queues.flag as flag')
            ->get();
        foreach($data as $dt){
            date_default_timezone_set("Asia/Jakarta");
            $sent_at = strtotime($dt->send_at);
            $now = strtotime("now");
            if($sent_at <= $now){
                $client = new Client();
                $send = $client->post('https://mg.gpfxseries.com/api/v1/mail-blast', [
                    'form_params' => [
                      'template' => $dt->templateName,
                      'name' => $dt->name,
                      'uuid' =>  (string)$dt->uuid,
                      'subject' => $dt->subject,
                      'email' => $dt->email
                    ]
                  ]);
                $flag= $dt->flag +1;
                DB::table('email_queues')->where('id', $dt->id)->update([
                    'is_sent' => 1,
                    'flag' => $flag
                ]);
                echo "Success send email to ". $dt->email. "<br>";
            }
        }
    }
}
