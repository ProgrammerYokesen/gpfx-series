<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Image;
use Cookie;
use Storage;

class DepositController extends Controller
{
    public function depositList(){
      $user_id = Session::get('user.id');
      $client = new Client();
      $account_list = $client->get(env('API_BASE_URL').'account_listing?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $accounts = json_decode($account_list->getBody());
      $mt4s = $accounts->data;

      //dd($mt4s);

      $bank_get = $client->get(env('API_BASE_URL').'bank_detail?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $bank = json_decode($bank_get->getBody());

      $client = new Client();
      $deposit_listing = $client->get(env('API_BASE_URL').'deposit_listing?user_id='.$user_id, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $depo = json_decode($deposit_listing->getBody());
      $depos = $depo->data;

      $client = new Client();
      $deposit_pending = $client->get(env('API_BASE_URL').'user_deposit_pending?user_id='.$user_id.'&status=pending', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $pending = json_decode($deposit_pending->getBody());
      //dd($pending);
      $pendings = $pending->data;

      $client = new Client();
      $converter_list = $client->get(env('API_BASE_URL').'converter_list', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $converter = json_decode($converter_list->getBody());
      $converters = $converter->data;

      $client = new Client();
      $tujuan_get = $client->get(env('API_BASE_URL').'bank_detail?user_id=5', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      $tujuan = json_decode($tujuan_get->getBody());

      return view('pages.deposit', compact('mt4s','bank','depos','pendings','converters','tujuan'));
    }

    function depositRequest(Request $request){
        try{
      if($request->file('deposit_proof'))
        {
          $image = $request->file('deposit_proof');
          $time = time();
          $img1  = str_replace(" ","",$time."-".$image->getClientOriginalName());
          $image_normal = Image::make($image)->widen(600, function ($constraint)
          {
            $constraint->upsize();
          });

          $image_thumb = Image::make($image)->fit(200, 200);
          $image_normal = $image_normal->stream();
          $image_thumb = $image_thumb->stream();

          Storage::disk('local')->put('public/'.$img1, $image_normal->__toString());
          Storage::disk('local')->put('public/thumbnails/'.$img1, $image_thumb->__toString());
        }
        else
        {
          $img1 = 'default.png';
        }
        //dd($request->cur);
        
        $amount_req = preg_replace("/[^0-9]/", "", $request->amount );
        $client = new Client();
        $converter_list = $client->get(env('API_BASE_URL').'converter_detail?id='.$request->cur, [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ]
        ]);

        $converter = json_decode($converter_list->getBody());
        //dd($converter);
        $converted = $amount_req / $converter->cur_ask;

      $user_id = Session::get('user.id');
      $client = new Client();
      $deposit_create = $client->post(env('API_BASE_URL').'deposit_create', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'user_id' => $user_id,
          'amount' => $amount_req,
          'currency' => $converter->cur_two,
          'approved_amount' => $converted,
          'currency_rate' => $converter->cur_ask,
          'photo' => $img1,
          'status' => 'pending',
          'bank_to' => $request->RadioBank,
          'to_bank' => $request->bankname,
          'username' => $request->banknumber,
          'signatures' => $request->bankaccount,
          'metatrader' => $request->metatrader
        ]
      ]);


      $dc = json_decode($deposit_create->getBody());

      if($dc->api_message=="success"){

        /**paket email**/
        /*
        $time_email = date('d-m-Y H:i');
        $name_email = Session::get('user.name');
        $email_email = Session::get('user.email');
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('mail.deposit_request', [
          'name' => $name_email,
          'email' => $email_email,
          'time' => $time_email,
          'tomt4id' => $request->metatrader,
          'amount' => $amount_req,
          'fromBank' => $request->bankname,
          'fromName' => $request->bankaccount,
          'fromNumber' => $request->banknumber
        ],
          function($message) use ($name_email, $email_email)
        {
            $message
              ->from('noreply@FinanciaFx.com','FinanciaFx Service')
                ->to($email_email,$name_email)
                  ->subject('Deposit Request FinanciaFx');
        });
        */
        /**sampai sini**/


        $client = new Client();
        $log = $client->post(env('API_BASE_URL').'log_create', [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ],
          'form_params' => [
            'user_id' => $user_id,
            'area' => 'user',
            'kegiatan' => 'deposit',
            'keterangan' => 'Pengajuan deposit dikirim',
          ]
        ]);

        Alert::success('Pengajuan deposit akan segera kami prosess','Berhasil ');
      }
        $respondLog = json_decode($log->getStatusCode());
      return response()->json($respondLog);
    }catch (\GuzzleHttp\Exception\RequestException $e){
         $response = 'failed';
        return response()->json($response);
        
    } 
    }
}
