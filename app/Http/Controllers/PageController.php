<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Session;
use GuzzleHttp\Client;

class PageController extends Controller
{
    public function homePage()
    {
        return view('pages.home');
    }
    
    public function helpPage()
    {
        return view('pages.help');
    }

    public function leaderboardPage()
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $klasemen = DB::table('users')
                    ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
                    ->join('signals', 'signals.login', 'trading_account.loginId')
                    ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
                    ->groupBy('users.id')
                    ->where('trading_account.type', 'GPFX Series')
                    ->limit(10)
                    ->whereBetween('trading_account.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->get()->sortByDesc('profit')->values();
        return view('pages.leaderboard', compact('klasemen'));
    }

    public function leaderboardGpPage()
    {
        $klasemen = DB::table('users')
            ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
            ->join('signals', 'signals.login', 'trading_account.loginId')
            ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
            ->groupBy('users.id')
            ->where('trading_account.type', 'GPFX Series')
            ->limit(10)
            ->get()
            ->sortByDesc('profit')->values();
        return view('pages.leaderboard-gp', compact('klasemen'));
    }
    
    public function demoKlasemen()
    {
        $user_id = Session::get('user.id');
        $klasemen = DB::table('users')
            ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
            ->join('signals', 'signals.login', 'trading_account.loginId')
            ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
            ->groupBy('users.id')
            ->where('trading_account.type', 'GPFX Series')
            ->limit(10);
        $klasemenTotal["klasemen"] = $klasemen->get()->sortByDesc('profit')->values();
        $checkTopTen = $klasemen->where('users.id', $user_id)->count();
        $checkTopTen = 0;
        if ($checkTopTen > 0) {
            $checkTopTen = 1;
        }
        $klasemenTotal['topTen'] = $checkTopTen;
        $klasemenTotal["selfData"] = $klasemen->where('users.id', $user_id)->first();

        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $klasemen = DB::table('users')
                    ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
                    ->join('signals', 'signals.login', 'trading_account.loginId')
                    ->select('users.id as id', 'users.name', 'users.email', 'trading_account.loginId as accountNumber', DB::raw("sum(signals.profit) as profit"))
                    ->groupBy('users.id')
                    ->where('trading_account.type', 'GPFX Series')
                    ->limit(10)
                    ->whereBetween('trading_account.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
        $klasemenMingguan["klasemen"] = $klasemen->get()->sortByDesc('profit')->values();
        $checkTopTen = $klasemenMingguan["klasemen"]->where('users.id', $user_id)->count();
        $topTen = 0;
        if ($topTen > 0) {
            $topTen = 1;
        }
        $klasemenMingguan['topTen'] = $topTen;
        $klasemenMingguan["selfData"] = $klasemen->where('users.id', $user_id)->first();
        return view('pages.demo-menu.demo-klasemen', compact('klasemenTotal', 'klasemenMingguan'));
    }
    
    public function materiPage()
    {
        $announcements = DB::table('materi')
            ->orderBy('created_at', 'asc')
            ->get();
        // dd($data);
        return view('pages.demo-menu.materi', compact('announcements'));
    }
    
    public function zoomPage()
    {
        $zooms = DB::table('zoom_meeting')->where('status', 'active')->orderBy('created_at', 'desc')->get();
        return view('pages.demo-menu.zoom', compact('zooms'));
    }
    
    public function telegramPage()
    {
        return view('pages.demo-menu.telegram');
    }

    public function forgotPass()
    {
        return view('pages.forgot-pass');
    }
    public function postForgotPass(Request $request){
        $client = new Client();
        $response = $client->post('https://mg.gpfxseries.com/api/v1/send-email', [
        'form_params' => [
            'email' => $request->email,
            ]
        ]);
        $result = json_decode($response->getBody());
        if($result->Status == "Success"){
            return redirect()->route('forgotPass')->with('Message', 'Email perubahan password telah dikirim, silahkan periksa pesan masuk email');
        }
        else{
            return redirect()->route('forgotPass')->with('Message', 'Gagal memverifikasi email');
        }
        
    
    }
    
    public function resetPass(Request $request)
    {
        if($request->has('token')){
            $token = $request->token;
        }
        return view('pages.reset-pass', compact('token'));
    }
    public function postResetPass(Request $request){
        $client = new Client();
        $response = $client->post('https://mg.gpfxseries.com/api/v1/forgot-password', [
        'form_params' => [
            'token' => $request->token,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
            ]
        ]);
        $result = json_decode($response->getBody());
        if($result->Status == "Success"){
            return redirect()->route('resetSuccess');
        }
        else{
            return redirect()->route('resetPass')->with('Message', 'Pergantian password gagal');
        }
    }

    public function resetSuccess()
    {
        return view('pages.reset-success');
    }
}
