<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Cookie;

class LoginController extends Controller
{
  public function login(){
    return view('template.master-login');
  }

  public function processLogin(Request $request){
    //dd($request);
    $client = new Client();
    $result = $client->post(env('API_BASE_URL').'login', [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ],
      'form_params' => [
          'email' => $request->input('email'),
          'password' => $request->input('password')
      ]
    ]);

    if($result->getStatusCode() == '200'){
      $res = json_decode($result->getBody());
      //dd($res);
      if($res->api_message=="success"){
        Session::put('user.id',$res->id);
        Session::put('user.name',$res->name);
        Session::put('user.photo',$res->photo);
        Session::put('user.email',$res->email);
        Session::put('user.id_cms_privileges',$res->id_cms_privileges);
        Session::put('user.cms_privileges_name',$res->cms_privileges_name);
        Session::put('user.parent',$res->parent);
        Session::put('user.username',$res->username);
        Session::put('user.status',$res->status);
        Session::put('user.new','old');
        $client = new Client();
        $log = $client->post(env('API_BASE_URL').'log_create', [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ],
          'form_params' => [
            'user_id' => $res->id,
            'area' => 'user',
            'kegiatan' => 'login',
            'keterangan' => 'Login berhasil',
          ]
        ]);

        Alert::success('You are Loged In!!','Welcome '.$res->name);
        switch($res->id_cms_privileges)
        {
          case '2' : Return redirect()->route('dashboard');break;
          /*
          case '3' : Return redirect()->route('financeIndex');break;
          case '4' : Return redirect()->route('settlementTrader');break;
          case '5' : Return redirect()->route('masterIndex');break;
          case '7' : Return redirect()->route('masterpartisi');break;
          case '8' : Return redirect()->route('masterpartisi');break;
          case '9' : Return redirect()->route('masterpartisi');break;
          case '90210' : Return redirect()->route('masterIndex');break;
          case '555' : Return redirect()->route('writerIndex');break;
          */
        }
      }else{
        Alert::success('Wrong Credentials','please check your login credentials');
        Return redirect()->route('masuk');
      }
    }
  }

  public function logout(Request $request){
    $request->session()->flush();
    Alert::success( 'You are Loged Out!!','See you!!');
    return redirect()->route('masuk');
  }
}
