<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use Alert;
use Validator;
use Redirect;
use Input;
use Cookie;

class RegisterController extends Controller
{
  public function register(){
    return view('template.master-daftar');
  }

  public function processRegister(Request $request){
    Session::flash('old_name', $request->name);
    Session::flash('old_email', $request->email);
    Session::flash('old_wa', $request->wa);

    $rules = array(
      'name' => 'required|min:3|max:60',
      'email' => 'required',
      'wa' => 'required',
      'password' => 'required',
      'g-recaptcha-response' => 'required|captcha'
    );

    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails())
    {
      return redirect()->route('daftar')->withInput()->withErrors($validator);
    }
    else
    {
      $client = new Client();
      $result = $client->get(env('API_BASE_URL').'register_check?email='.$request->email, [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ]
      ]);

      if($request->ref){
        $ref = $request->ref;
      }elseif($request->ref == ''){
        $ref = '0';
      }else{
        $ref = '0';
      }

      if($request->campaign){
        $campaign = $request->campaign;
      }elseif($request->campaign == ''){
        $campaign = '0';
      }else{
        $campaign = '0';
      }

      if($request->so){
        $so = $request->so;
      }elseif($request->so == ''){
        $so = '0';
      }else{
        $so = 'organic';
      }

      if($request->uuid){
        $uuid = $request->uuid;
      }elseif($request->uuid == ''){
        $uuid = '0';
      }else{
        $uuid = md5('nothing');
      }

      $phone = $request->wa;

      $res = json_decode($result->getBody());

      if($res->api_message!="success"){
        $client = new Client();
        $register = $client->post(env('API_BASE_URL').'register', [
          'headers' => [
            'X-Authorization-Token' => md5(env('API_KEY')),
            'X-Authorization-Time'  => time()
          ],
          'form_params' => [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'id_cms_privileges' => '2',

            'disclaimer' => $request->disclaimer,
            'status' => 'active',
            'parent' => $ref,
            'uuid' => $uuid,
            'referal' => $request->dari,
            'origin' => $so,
            'campaign' => $campaign
          ]
        ]);

        $reg = json_decode($register->getBody());

        if($reg->api_message=="success"){
          $client = new Client();
          $profile = $client->post(env('API_BASE_URL').'create_profile', [
            'headers' => [
              'X-Authorization-Token' => md5(env('API_KEY')),
              'X-Authorization-Time'  => time()
            ],
            'form_params' => [
              'fullname' => $request->name,
              'email' => $request->email,
              'phone' => $phone,
              'status' => 'active',
              'user_id' =>  $reg->id,
              'parent' => $ref,
              'origin' => $so,
              'campaign' => $campaign
            ]
          ]);

          /**paket email**/
          /*
          $time_email = date('d-m-Y H:i');
          $name_email = $request->name;
          $email_email = $request->email;
          $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
          $beautymail->send('mail.register_first', [
            'name' => $request->name,
            'email' => $request->email,
            'time' => $time_email,
            'referall' => $request->parent
          ],
            function($message) use ($name_email, $email_email)
          {
              $message
                ->from('noreply@FinanciaFx.com','FinanciaFx Service')
                  ->to($email_email,$name_email)
                    ->subject('FinanciaFx Registration');
          });
          */
          /**sampai sini**/


          $prof = json_decode($profile->getBody());

          if($prof->api_message=="success"){
            $client = new Client();
            $bank = $client->post(env('API_BASE_URL').'create_bank', [
              'headers' => [
                'X-Authorization-Token' => md5(env('API_KEY')),
                'X-Authorization-Time'  => time()
              ],
              'form_params' => [
                'account_name' => $request->name,
                'user_id' => $reg->id
              ]
            ]);

            $client = new Client();

            $wallet = $client->post(env('API_BASE_URL').'wallet_create', [
              'headers' => [
                'X-Authorization-Token' => md5(env('API_KEY')),
                'X-Authorization-Time'  => time()
              ],
              'form_params' => [
                'name' => $request->name,
                'user_id' => $reg->id,
                'amount' => '0',
                'signatures' => md5($request->name.$reg->id),
              ]
            ]);

            $client = new Client();
            $log = $client->post(env('API_BASE_URL').'log_create', [
              'headers' => [
                'X-Authorization-Token' => md5(env('API_KEY')),
                'X-Authorization-Time'  => time()
              ],
              'form_params' => [
                'user_id' => $reg->id,
                'area' => 'user',
                'kegiatan' => 'register',
                'keterangan' => 'Registrasi Berhasil',
              ]
            ]);

            $client = new Client();
            $result = $client->post(env('API_BASE_URL').'login', [
              'headers' => [
                'X-Authorization-Token' => md5(env('API_KEY')),
                'X-Authorization-Time'  => time()
              ],
              'form_params' => [
                  'email' => $request->email,
                  'password' => $request->password
              ]
            ]);

            if($result->getStatusCode() == '200'){
              $res = json_decode($result->getBody());
              if($res->api_message=="success"){
                Session::put('user.id',$res->id);
                Session::put('user.name',$res->name);
                Session::put('user.photo',$res->photo);
                Session::put('user.email',$res->email);
                Session::put('user.id_cms_privileges',$res->id_cms_privileges);
                Session::put('user.cms_privileges_name',$res->cms_privileges_name);
                Session::put('user.parent',$res->parent);
                Session::put('user.username',$res->username);
                Session::put('user.status',$res->status);
                Session::put('user.new','new');
                Alert::success('You are Loged In!!','Welcome '.$res->name);
                $lemparFullname = $res->name;
                $lemparEmail = $res->email;
                $berhasil = $res->id;
                $welcome = 'newclient';
                Return redirect()->route('dashboard',compact('berhasil','welcome'));

              }else{
                Alert::success('Wrong Credentials','please check your login credentials');
                Return redirect()->route('masuk');
              }
            }
          }else{
            Alert::success('Pendaftaran gagal','hubungi customer service kami');
            Return redirect()->route('daftar');
          }
        }else{
          Alert::success('Pendaftaran gagal','hubungi customer service kami');
          Return redirect()->route('daftar');
        }
      }else{
        Alert::success('Silahkan Login','Anda sudah terdaftar '.$request->email);
        Return redirect()->route('masuk');
      }
    }
  }
}
