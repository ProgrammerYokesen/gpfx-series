<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\memberExport;
use App\activity;
use DB;
use CRUDBooster;
class adminController extends Controller
{
    public function userData(){
        // $client = new Client();
        // $result = $client->get('http://fed.financiafx.com/api/gpfxseries_register');
        // $result = json_decode($result->getBody());
        // $result = $result->data;
        // foreach($result as $res){
        //     $connection = new Client();
        //     $result = $connection->get('http://fed.financiafx.com/api/profile_detail?user_id='.$res->id);
        //     $userDetail = json_decode($result->getBody());
        //     DB::table('users')->updateOrInsert([
        //          'uuid' => $res->id,
        //         ],
        //         [
        //         'name' => $res->name,
        //         'email' => $res->email,
        //         'campaign' => $res->campaign,
        //         'phone' => $userDetail->phone
        //         ]);
        // }
        $result = DB::table('users')->orderBy('uuid', 'desc')->get();
 
        return view('admin.user-data', compact('result'));
    }
    
    // public function refreshMember(){
    //     $client = new Client();
    //     $result = $client->get('http://fed.financiafx.com/api/gpfxseries_register');
    //     $result = json_decode($result->getBody());
    //     $result = $result->data;
    //     foreach($result as $res){
    //         $connection = new Client();
    //         $result = $connection->get('http://fed.financiafx.com/api/profile_detail?user_id='.$res->id);
    //         $userDetail = json_decode($result->getBody());
    //         DB::table('users')->updateOrInsert([
    //              'uuid' => $res->id,
    //             ],
    //             [
    //             'name' => $res->name,
    //             'email' => $res->email,
    //             'campaign' => $res->campaign,
    //             'phone' => $userDetail->phone
    //             ]);
    //     }
    //     return redirect()->back();
    // }
    
    public function signals(){
        $client = new Client();
        $result = $client->get('https://api.traderindo.com/api/signals?cattegory=traderindo');
        $signals = json_decode($result->getBody());
        return view('admin.signals', compact('signals'));
    }
    
    public function klasemen(){
        $datas = DB::table('users')
                ->leftJoin('trading_account', 'trading_account.uuid', 'users.uuid')
                ->join('signals', 'signals.login', 'trading_account.loginId')
                ->select('users.id as id', 'users.name','users.email', DB::raw("sum(signals.profit) as profit"))
                ->groupBy('users.id')
                ->get();
        return view('admin.klasemen', compact('datas'));
    }
    
    public function userdataDetail($id){
        $new_user = array();
        $client = new Client();
        $result = $client->get('http://fed.financiafx.com/api/profile_detail?user_id='.$id);
        $userDetail = json_decode($result->getBody());
        
        $client = new Client();
        $result = $client->get('http://fed.financiafx.com/api/gpfxseries_register');
        $result = json_decode($result->getBody());
        $user = $result->data;
        $user = collect($user);
        $user = $user->where('id', $id)->first();
        $activities = activity::join('cms_users', 'cms_users.id', 'activities.admin_id')
        ->where('activities.user_id', $id)
        ->select('cms_users.name', 'activities.activity','activities.user_id','activities.created_at','activities.updated_at')
        ->get();
        return view('admin.user-detail', compact('user','userDetail','activities'));
    }
    
    public function exportExcel(){
        return Excel::download(new memberExport, 'member.xlsx');
    }
    
    public function selectedData($array){
        $new_array = array();
        foreach($array as $key => $val){
            $new_array[$key]['id'] = $val->id;
            $new_array[$key]['name'] = $val->name;
            $new_array[$key]['email'] = $val->email;
            $new_array[$key]['campaign'] = $val->campaign;
            $new_array[$key]['status'] = $val->status;
        }
        return $new_array;
    }
    
    public function processActivity(Request $request){
        date_default_timezone_set("Asia/Bangkok");
        \DB::table('activities')->insert([
            'user_id' => $request->user_id,
            'admin_id' => $request->admin_id,
            'activity' => $request->activity,
            'created_at' => now(),
            'updated_at' => now()
            ]);
        return redirect()->back();
    }
    
    public function refreshMember(){
                $client = new Client();
        $result = $client->get('http://fed.financiafx.com/api/gpfxseries_register');
        $result = json_decode($result->getBody());
        $result = $result->data;
        // dd($result);
        foreach($result as $res){
            $connection = new Client();
            $result = $connection->get('http://fed.financiafx.com/api/profile_detail?user_id='.$res->id);
            $userDetail = json_decode($result->getBody());
            // dd($userDetail);
            DB::table('users')->updateOrInsert([
                 'uuid' => $res->id,
                ],
                [
                'name' => $res->name,
                'email' => $res->email,
                'campaign' => $res->campaign,
                'phone' => $userDetail->phone
                ]);
            //   dd("cek");
        }
        $datas = DB::table('users')->select('uuid')->get();
        foreach($datas as $data){
            $guzzle = new Client();
            $account_list = $guzzle->get(env('API_BASE_URL').'account_listing?user_id='.$data->uuid, [
                    'headers' => [
                         'X-Authorization-Token' => md5(env('API_KEY')),
                         'X-Authorization-Time'  => time()
                    ]
            ]);
                
            $accounts = json_decode($account_list->getBody());
            $lists = $accounts->data;
            foreach($lists as $list){
                DB::table('trading_account')->updateOrInsert([
                    'loginId' => $list->login
                    ],
                    [
                    'uuid' => $data->uuid,
                    'type' => $list->type,
                    'status' => $list->status
                        ]);
            }
        }
        return redirect()->back();
    }
    
    public function changeUserStatus(Request $request){
        DB::table('users')->where('id', $request->id)->update([
            'status' => $request->status,
            'parent' => CRUDBooster::myID()
            ]);
        return redirect()->back();
    }
      
     public function followUp(Request $request){
        $id = DB::table('activities')->where('user_id', $request->user_id)->orderBy('id', 'desc')->first();
        if($id){
            DB::table('activities')->where('id', $id->id)->update([
            'next_fu' => $request->next_fu
            ]);
        }
        else{
            DB::table('activities')->insert([
            'admin_id' => CRUDBooster::myID(),
            'user_id' => $request->user_id,
            'next_fu' => $request->next_fu
            ]);
        }

        return redirect()->back();
    }

    public function emailQueue(){
        $datas = DB::table('users')->select('email')->get();
        $templates = DB::table('email_templates')->select('id', 'name')->get();
        return view('custom-element.input-email', compact('datas', 'templates'));
    }
    
    public function processEmailQueue(Request $request){
        // dd($request);
        if($request->custom_field[0] == "all"){
            $mailList = DB::table('users')->select('email')->get();
            foreach($mailList as $mail){
                dd($mailList);
                DB::table('email_queues')->insert([
                    'email_template_id' => $request->email_template_id,
                    'send_at' => $request->send_at,
                    'type' => $request->type,
                    'is_sent' => 0,
                    'email_recipient' => $mail->email
                ]);
            }
            return redirect(env('app_url').'/competitionadmin/email_queues');
        }
        else{
            foreach($request->custom_field as $mail){
                    DB::table('email_queues')->insert([
                        'email_template_id' => $request->email_template_id,
                        'send_at' => $request->send_at,
                        'type' => $request->type,
                        'is_sent' => 0,
                        'email_recipient' => $mail
                    ]);
            }
            return redirect(env('app_url').'/competitionadmin/email_queues');
        }
    }

}
