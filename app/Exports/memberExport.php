<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use GuzzleHttp\Client;

class memberExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $client = new Client();
        $result = $client->get('http://fed.financiafx.com/api/gpfxseries_register');
        $result = json_decode($result->getBody());
        $result = $result->data;
        $data = $this->selectedData($result);
        return collect($data);
    }
        
    public function selectedData($array){
        $new_array = array();
        foreach($array as $key => $val){
            $client = new Client();
            $result = $client->get('http://fed.financiafx.com/api/profile_detail?user_id='.$val->id);
            $userDetail = json_decode($result->getBody());
        
            $new_array[$key]['id'] = $val->id;
            $new_array[$key]['name'] = $val->name;
            $new_array[$key]['email'] = $val->email;
            $new_array[$key]['phone'] = $userDetail->phone;
            $new_array[$key]['address'] = $userDetail->address;
            $new_array[$key]['campaign'] = $val->campaign;
            $new_array[$key]['status'] = $val->status;
        }
        return $new_array;
    }
}
