@extends('template.master')
@section('content')
    <div class="lab-title">pengumuman</div>
    <div class="announcement-list">
        @foreach ($announcements as $item)
            <a type="button" data-toggle="modal" data-target="#announcementModal{{ $item->id }}">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="https://gpfxseries.com/{{ $item->photo }}" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">{{ $item->title }}</p>
                        <p class="card-text">Published in : {{ $item->created_at }}</p>
                    </div>
                </div>
            </a>

            <div class="modal fade" id="announcementModal{{ $item->id }}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{ $item->title }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <img class="card-img-modal"
                                    src="https://www.cyclonis.com/images/2018/05/incognito-browser-mode.jpg"
                                    alt="Card image cap">
                            </div>
                            <div>
                                <p>{{ $item->description }}</p>
                                <a href="/{{ $item->attachment }}" download="{{$item->attachment}}" class="mt-2 btn btn-primary">Download Attachment</a>
                                <p class="card-text mt-4">Published in : {{ $item->created_at }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </div>


@endsection
