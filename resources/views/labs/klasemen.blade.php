@extends('template.master')
@section('content')
    <div class="lab-title">klasemen</div>
    <div class="rank-table mt-3" id="weekly">
        <div class="row justify-content-between px-3 mb-2">
            <div class="lab-subtitle" style="align-self: center;">KLASEMEN MINGGUAN</div>
            <button class="btn btn-primary" onclick="changeTable('total')">KLASEMEN TOTAL</button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">rank</th>
                    <th scope="col">nama</th>
                    <th scope="col">account number</th>
                    <th scope="col">equity</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 1; @endphp
                @forelse($klasemenMingguan["klasemen"] as $dt)
                <tr>
                    <th scope="row">{{ $i }}</th>
                    <td>{{ $dt->name }}</td>
                    <td>{{ $dt->accountNumber }}</td>
                    <td>{{ $dt->profit }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" class="text-center">Data Kosong</td>
                </tr>
                @php $i++; @endphp
                @endforelse
                @if ($klasemenMingguan["topTen"] == 1)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>...</td>
                    <td>{{ $klasemenMinggaun["selfData"]->name }}</td>
                    <td>{{ $klasemenMinggaun["selfData"]->accountNumber }}</td>
                    <td>{{ $klasemenMinggaun["selfData"]->profit }}</td>
                </tr>                    
                @endif
            </tbody>
        </table>
    </div>

    <div class="rank-table mt-3" id="total">
        <div class="row justify-content-between px-3 mb-2">
            <div class="lab-subtitle" style="align-self: center;">KLASEMEN TOTAL</div>
            <button class="btn btn-primary" onclick="changeTable('weekly')">KLASEMEN MINGGUAN</button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">rank</th>
                    <th scope="col">nama</th>
                    <th scope="col">account number</th>
                    <th scope="col">equity</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 1; @endphp
                @forelse($klasemenTotal["klasemen"] as $dt)
                <tr>
                    <th scope="row">{{ $i }}</th>
                    <td>{{ $dt->name }}</td>
                    <td>{{ $dt->accountNumber }}</td>
                    <td>{{ $dt->profit }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="4" class="text-center">Data Kosong</td>
                </tr>
                @php $i++; @endphp
                @endforelse
                @if ($klasemenTotal["topTen"] == 1)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>...</td>
                    <td>{{ $klasemenTotal["selfData"]->name }}</td>
                    <td>{{ $klasemenTotal["selfData"]->accountNumber }}</td>
                    <td>{{ $klasemenTotal["selfData"]->profit }}</td>
                </tr>                    
                @endif
            </tbody>
        </table>
    </div>

    <script>
        function changeTable(tableID) {

            if(tableID === "total"){
                $("#weekly").css("visibility","hidden")
                $("#weekly").css("opacity","0")
                $("#weekly").hide()
            }else{
                $("#total").css("visibility","hidden")
                $("#total").css("opacity","0")
                $("#total").hide()
            }
            $("#"+tableID).show()
            $("#"+tableID).css("visibility","visible")
            $("#"+tableID).css("opacity","1")
        }

    </script>
@endsection
