<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Grand Prix Series Forex Trading Indonesia</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('./images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./images/favicon/favicon-16x16.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-7HLQ2YERJB"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-7HLQ2YERJB');

    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '507042357408304');
        fbq('track', 'PageView');

    </script>
    <meta name="facebook-domain-verification" content="z06jkhhsh9vcqp3ljwuj1mmsm1yueq" />

</head>

<body>
    <!-- HEADER -->
    <div class="header_container">
        <div class="container">
            <a href="{{ route('homePage') }}">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="header_logo">
            </a>
            {{-- <div class="logo" id="logo"></div> --}}
            <div class="menu">
                <a href="{{ route('leaderboardPage') }}" class="menu_klasemen">KLASEMEN</a>
                <a href="{{ route('masuk') }}">
                    <button>MASUK</button>
                </a>
            </div>
        </div>
    </div>

    <!-- HERO / JUMBOTRON -->
    <div class="hero_container">
        <div class="container">
            <h1>GRANDPRIX</h1>
            <h2>Series Trading Indonesia</h2>
            <h5>
                Ikuti dan menangkan hadiah total senilai 70 juta rupiah! Sudah pasti
                akan ada pemenang setiap minggunya dan juga nantikan hadiah sebagai
                juara umum yang akan diumumkan di akhir periode grand prix series
                trading indonesia!
            </h5>
            <a href="{{ route('daftar') }}">
                <button>Daftar Sekarang</button>
            </a>
        </div>
    </div>

    <!-- TABLE -->
    <div class="table_container">
        <div class="container">
            <h2>TOTAL HADIAH GRAND PRIX</h2>
            <div class="text-center">
                <img src="{{ asset('images/hadiah.png') }}" alt="hadiah" class="hadiah_img img-fluid">
            </div>
            <ul class="nav nav-pills justify-content-center mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-weekly-tab" data-toggle="pill" href="#pills-weekly" role="tab"
                        aria-controls="pills-weekly" aria-selected="true">WEEKLY</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-champion-tab" data-toggle="pill" href="#pills-champion" role="tab"
                        aria-controls="pills-champion" aria-selected="false">CHAMPION</a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">
                <!-- WEEKLY POINT -->
                <div class="tab-pane fade show active table-responsive" id="pills-weekly" role="tabpanel"
                    aria-labelledby="pills-weekly-tab">
                    <div class="rank_container" style="margin-top: 25px">
                        <div class="rank_flex">
                            <div class="rank_left">1st Place</div>
                            <div class="rank_right">Rp1.000.000 + 25 <img src="{{ asset('images/GP.svg') }}"
                                    alt="point">
                            </div>
                        </div>
                    </div>

                    <div class="rank_container">
                        <div class="rank_flex">
                            <div class="rank_left">2ND PLACE</div>
                            <div class="rank_right">Rp750.000 + 18 <img src="{{ asset('images/GP.svg') }}"
                                    alt="point">
                            </div>
                        </div>
                        <div class="rank_flex">
                            <div class="rank_left">3RD PLACE</div>
                            <div class="rank_right">Rp250.000 + 15 <img src="{{ asset('images/GP.svg') }}"
                                    alt="point">
                            </div>
                        </div>
                    </div>

                    <div class="rank_container">
                        <div class="rank_flex">
                            <div class="rank_left">4TH - 8TH PLACE</div>
                            <div class="rank_right">Rp100.000 + 12 - 4 <img src="{{ asset('images/GP.svg') }}"
                                    alt="point">
                            </div>
                        </div>
                        <div class="rank_flex">
                            <div class="rank_left">9TH - 10TH PLACE</div>
                            <div class="rank_right">2 - 1 <img src="{{ asset('images/GP.svg') }}" alt="point">
                            </div>
                        </div>
                    </div>

                    <div class="rank_notes">
                        <h5>*</h5>
                        <img src="{{ asset('images/GP.svg') }}" alt="point">
                        <h5>adalah point yang diperoleh untuk menjadi Juara Umum Grand Prix</h5>
                    </div>

                </div>

                <!-- CHAMPION TABLE -->
                <div class="tab-pane fade table-responsive" id="pills-champion" role="tabpanel"
                    aria-labelledby="pills-champion-tab">

                    <div class="rank_container" style="margin-top: 25px">
                        <div class="rank_flex">
                            <div class="rank_left">1st Place</div>
                            <div class="rank_right">Rp15.000.000,-</div>
                        </div>
                    </div>

                    <div class="rank_container">
                        <div class="rank_flex">
                            <div class="rank_left">2ND PLACE</div>
                            <div class="rank_right">Rp7.500.000,-</div>
                        </div>
                        <div class="rank_flex">
                            <div class="rank_left">3RD PLACE</div>
                            <div class="rank_right">Rp2.500.000,-</div>
                        </div>
                    </div>

                    <div class="rank_container">
                        <div class="rank_flex">
                            <div class="rank_left">4TH PLACE</div>
                            <div class="rank_right">Rp1.500.000,-</div>
                        </div>
                        <div class="rank_flex">
                            <div class="rank_left">5TH - 10TH PLACE</div>
                            <div class="rank_right">Rp1.000.000,-</div>
                        </div>
                    </div>

                    {{-- <div class="rank_notes">
                        <h5>*</h5>
                        <img src="{{ asset('images/GP.svg') }}" alt="point">
                        <h5>adalah point yang diperoleh untuk menjadi Juara Umum Grand Prix</h5>
                    </div> --}}

                    {{-- <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Type</th>
                                <th scope="col">Juara</th>
                                <th scope="col">Total Hadiah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Grand Champion</th>
                                <td>1</td>
                                <td>Rp15.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>2</td>
                                <td>Rp7.500.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>3</td>
                                <td>Rp2.500.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>4</td>
                                <td>Rp1.500.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>5</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>6</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>7</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>8</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>9</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                            <tr>
                                <th scope="row"></th>
                                <td>10</td>
                                <td>Rp1.000.000,-</td>
                            </tr>
                        </tbody>
                    </table> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- END TABLE -->

    <!-- HOW TO JOIN -->
    <div class="join_container">
        <div class="container">
            <h2>CARA BERGABUNG</h2>
            <img src="{{ asset('images/join.svg') }}" alt="cara join" class="join-d" />
            <img src="{{ asset('images/join-mobile.svg') }}" alt="cara join" class="join-mb" />
            <div class="text-center">
                <a href="{{ route('daftar') }}">
                    <button>DAFTAR SEKARANG</button>
                </a>
            </div>
        </div>
    </div>
    <!-- END HOW TO JOIN -->

    <!-- FAQ -->
    <div class="faq_container">
        <div class="container">
            <h2>Frequently Asked Questions</h2>
            <div class="faq_border">

                <!-- FAQ 1 -->
                <div class="faq">
                    <div class="question_container" data-toggle="collapse" href="#faq1" role="button"
                        aria-expanded="false" aria-controls="faq1">
                        <h5>Kapan pemenang dari Grand Prix Series akan diumumkan?</h5>
                        <img src="{{ asset('images/caret.svg') }}" alt="expand" />
                    </div>
                    <div class="collapse faq_answer" id="faq1">
                        <div>
                            <h5 class="mb-4">
                                Grand Prix Series weekly akan diumumkan pada hari Sabtu setiap
                                minggunya di closing market.
                            </h5>
                            <h5>
                                Grand Prix Series total akan diumumkan pada Rabu, 13 Oktober 2021
                                setelah 15 series telah selesai diadakan.
                            </h5>
                        </div>
                    </div>
                </div>
                {{-- <hr /> --}}
                <!-- END FAQ 1 -->

                <!-- FAQ 2 -->
                <div class="faq">
                    <div class="question_container" data-toggle="collapse" href="#faq2" role="button"
                        aria-expanded="false" aria-controls="faq2">
                        <h5>Berapa lama Grand Prix Series ini akan berlangsung?</h5>
                        <img src="{{ asset('images/caret.svg') }}" alt="expand" />
                    </div>
                    <div class="collapse faq_answer" id="faq2">
                        <div>
                            <h5 class="mb-4">
                                Grand Prix mingguan akan diadakan selama 15 minggu yang terdiri
                                dari 1 series dengan pemilihan pair yang berbeda-beda setiap
                                minggunya. Berikut adalah jadwal dari Grand Prix Series mingguan:
                            </h5>
                            <div class="faq_jadwal">
                                <img src="{{ asset('images/jadwal-1.png') }}" alt="jadwal" />
                                <img src="{{ asset('images/jadwal-2.png') }}" alt="jadwal" />
                                <img src="{{ asset('images/jadwal-3.png') }}" alt="jadwal" />
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <hr /> --}}
                <!-- END FAQ 2 -->

                <!-- FAQ 3 -->
                <div class="faq">
                    <div class="question_container" data-toggle="collapse" href="#faq3" role="button"
                        aria-expanded="false" aria-controls="faq3">
                        <h5>Siapa yang dapat memenangkan Grand Prix Series?</h5>
                        <img src="{{ asset('images/caret.svg') }}" alt="expand" />
                    </div>
                    <div class="collapse faq_answer" id="faq3">
                        <div>
                            <h5 class="mb-4">
                                Pemenang akan ditentukan berdasarkan klasemen terakhir di setiap
                                minggu. Perhitungan klasemen akan berurutan berdasarkan poin yang
                                didapatkan dari ROI (Return On Investment), dan perhitungan ROI
                                akan kembali menjadi 0% di setiap minggunya.
                            </h5>
                            <h5>
                                Hanya transaksi dipair yang sesuai dengan jadwal pair yang telah
                                ditentukan yang akan dihitung sebagai ROI (Return Of Investment)
                                untuk perolehan klasemen dan Transaksi di luar pair yang ada di
                                jadwal setiap seriesnya tidak akan dihitung sebagai ROI (Return Of
                                Investment) untuk klasemen.
                            </h5>
                        </div>
                    </div>
                </div>
                {{-- <hr /> --}}
                <!-- END FAQ 3 -->

                <!-- FAQ 4 -->
                <div class="faq">
                    <div class="question_container" data-toggle="collapse" href="#faq4" role="button"
                        aria-expanded="false" aria-controls="faq4">
                        <h5>Siapa yang dapat memenangkan Grand Champion?</h5>
                        <img src="{{ asset('images/caret.svg') }}" alt="expand" />
                    </div>
                    <div class="collapse faq_answer" id="faq4">
                        <div>
                            <h5 class="mb-4">
                                Peserta mengumpulkan point Grand Prix dari series mingguan untuk
                                diakumulasikan dalam perolehan point sebagai Grand Champion
                                diakhir periode
                            </h5>
                        </div>
                    </div>
                </div>
                {{-- <hr /> --}}
                <!-- END FAQ 4 -->
            </div>
        </div>
    </div>
    <!-- END FAQ -->

    <!-- FOOTER -->
    <div class="footer">
        <div class="container">
            <div class="footer_container">
                <div>
                    <h2>Ikuti sosial media Traderindo</h2>
                    <div class="footer_socmed">
                        <a href="https://www.facebook.com/groups/komunitastraderindo" target="_blank">
                            <img src="{{ asset('images/fb.svg') }}" alt="fb">
                        </a>
                        <a href="https://www.youtube.com/channel/UCTVy3OUjPQSsl68cgiDbU2g" target="_blank">
                            <img src="{{ asset('images/yt.svg') }}" alt="yt">
                        </a>
                        <a href="https://www.instagram.com/traderindo.official/" target="_blank">
                            <img src="{{ asset('images/ig.svg') }}" alt="ig">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <script>
        $(function() {
            $(document).scroll(function() {
                var $nav = $(".header_container");
                var $menu = $(".menu_klasemen");
                var $logo = $("#logo");

                $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                $menu.toggleClass("scrolled", $(this).scrollTop() > $nav.height());

                if ($(window).scrollTop() > $nav.height()) {
                    $logo.removeClass("logo").addClass("logo-scrolled");
                } else {
                    $logo.removeClass("logo-scrolled").addClass("logo");
                }
            });
        });

    </script>

    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=507042357408304&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
</body>

</html>
