@extends('template.master')
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','Klasemen')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="lab-title">klasemen</div>
    <div class="rank-table mt-3" id="weekly">
        <div class="row justify-content-between px-3 mb-2">
            <div class="lab-subtitle" style="align-self: center;">KLASEMEN MINGGUAN</div>
            <button class="btn btn-primary" onclick="changeTable('total')">KLASEMEN TOTAL</button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">rank</th>
                    <th scope="col">nama</th>
                    <th scope="col">account number</th>
                    <th scope="col">equity</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 1; @endphp
                @forelse($klasemenMingguan["klasemen"] as $dt)
                <tr>
                    <th scope="row">{{ $i }}</th>
                    <td>{{ $dt->name }}</td>
                    <td>{{ $dt->accountNumber }}</td>
                    <td>{{ $dt->profit }}</td>
                </tr>
                @php $i++; @endphp
                @empty
                <tr>
                    <td colspan="4" class="text-center">Data Kosong</td>
                </tr>

                @endforelse
                @if ($klasemenMingguan["topTen"] == 1)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>...</td>
                    <td>{{ $klasemenMingguan["selfData"]->name }}</td>
                    <td>{{ $klasemenMingguan["selfData"]->accountNumber }}</td>
                    <td>{{ $klasemenMingguan["selfData"]->profit }}</td>
                </tr>                    
                @endif
            </tbody>
        </table>
    </div>

    <div class="rank-table mt-3" id="total">
        <div class="row justify-content-between px-3 mb-2">
            <div class="lab-subtitle" style="align-self: center;">KLASEMEN TOTAL</div>
            <button class="btn btn-primary" onclick="changeTable('weekly')">KLASEMEN MINGGUAN</button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">rank</th>
                    <th scope="col">nama</th>
                    <th scope="col">account number</th>
                    <th scope="col">equity</th>
                </tr>
            </thead>
            <tbody>
                @php $i = 1; @endphp
                @forelse($klasemenTotal["klasemen"] as $dt)
                <tr>
                    <th scope="row">{{ $i }}</th>
                    <td>{{ $dt->name }}</td>
                    <td>{{ $dt->accountNumber }}</td>
                    <td>{{ $dt->profit }}</td>
                </tr>
                @php $i++; @endphp
                @empty
                <tr>
                    <td colspan="4" class="text-center">Data Kosong</td>
                </tr>
                
                @endforelse
                @if ($klasemenTotal["topTen"] == 1)
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>...</td>
                    <td>{{ $klasemenTotal["selfData"]->name }}</td>
                    <td>{{ $klasemenTotal["selfData"]->accountNumber }}</td>
                    <td>{{ $klasemenTotal["selfData"]->profit }}</td>
                </tr>                    
                @endif
            </tbody>
        </table>
    </div>

    <script>
        function changeTable(tableID) {

            if(tableID === "total"){
                $("#weekly").css("visibility","hidden")
                $("#weekly").css("opacity","0")
                $("#weekly").hide()
            }else{
                $("#total").css("visibility","hidden")
                $("#total").css("opacity","0")
                $("#total").hide()
            }
            $("#"+tableID).show()
            $("#"+tableID).css("visibility","visible")
            $("#"+tableID).css("opacity","1")
        }

    </script>
@endsection


@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection