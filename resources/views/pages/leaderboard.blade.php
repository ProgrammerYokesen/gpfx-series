<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Grand Prix Series Forex Trading Indonesia</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('./images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./images/favicon/favicon-16x16.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
</head>

<body>
    <!-- HEADER -->
    <div class="header_container">
        <div class="container">
            <a href="{{ route('homePage') }}">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="header_logo">
            </a>
            {{-- <div class="logo" id="logo"></div> --}}
            <div class="menu">
                <a href="{{ route('leaderboardPage') }}" class="menu_klasemen" style="color: #ed0002">KLASEMEN</a>
                <a href="{{ route('masuk') }}">
                    <button>MASUK</button>
                </a>
            </div>
        </div>
    </div>

    <div class="leaderboard__container">
        <img src="{{ asset('images/leaderboard/trophy-l.png') }}" alt="" class="img-fluid trophy-l">
        <img src="{{ asset('images/leaderboard/trophy-r.png') }}" alt="" class="img-fluid trophy-r">
        <div class="container">
            <div class="leaderboard">
                <h1>Klasemen Mingguan</h1>
                <a href="{{ route('leaderboardGpPage') }}">
                    <button class="join_event">Klasemen Total</button>
                </a>
                @if($klasemen->count() > 0)
                <div class="leaderboard_podium">

                    <div class="winner winner-2">
                        <div>
                            <img src="{{ asset('images/leaderboard/winner2.svg') }}" alt="">
                        </div>
                        {{-- <div class="winner_thumb">
                                <img src="{{ $leaderboard[1]->photo }}" alt="">
                            </div> --}}
                        <h5 class="winner_name">{{$klasemen[1]->name}}</h5>
                        <h4 class="winner_likes">{{$klasemen[1]->accountNumber}}</h4>
                        <h5 class="likes">{{$klasemen[1]->profit}}</h5>
                    </div>

                    <div class="winner winner-1">
                        <div>
                            <img src="{{ asset('images/leaderboard/winner1.svg') }}" alt="">
                        </div>
                        {{-- <div class="winner_thumb">
                                <img src="{{ $leaderboard[0]->photo }}" alt="">
                            </div> --}}
                        <h5 class="winner_name">{{$klasemen[0]->name}}</h5>
                        <h4 class="winner_likes">{{$klasemen[0]->accountNumber}}</h4>
                        <h5 class="likes">{{$klasemen[0]->profit}}</h5>


                    </div>

                    <div class="winner winner-3">
                        <div>
                            <img src="{{ asset('images/leaderboard/winner3.svg') }}" alt="">
                        </div>
                        {{-- <div class="winner_thumb">
                                <img src="{{ $leaderboard[2]->photo }}" alt="">
                            </div> --}}
                        <h5 class="winner_name">{{$klasemen[2]->name}}</h5>
                        <h4 class="winner_likes">{{$klasemen[2]->accountNumber}}</h4>
                        <h5 class="likes">{{$klasemen[2]->profit}}</h5>


                    </div>

                </div>
                @endif
                <div class="leaderboard_table desktop">
                    <table>
                        <thead>
                            <tr>
                                <th>Rank</th>
                                <th>Nama</th>
                                <th>Account Number</th>
                                <th>Equity</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $i = 4; @endphp
                        @forelse($klasemen as $key => $value)
                        @php echo $key; @endphp
                        @if($key>2)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->accountNumber}}</td>
                                <td>{{$value->profit}}</td>
                            </tr>
                        @php $i++; @endphp
                        @endif
                        @empty
                        <tr>
                            <td colspan="4" class="text-center">Data Kosong</td>
                        </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>




    <!-- FOOTER -->
    <div class="footer">
        <div class="container">
            <div class="footer_container">
                <div>
                    <h2>Ikuti sosial media Traderindo</h2>
                    <div class="footer_socmed">
                        <a href="https://www.facebook.com/groups/komunitastraderindo" target="_blank">
                            <img src="{{ asset('images/fb.svg') }}" alt="fb">
                        </a>
                        <a href="https://www.youtube.com/channel/UCTVy3OUjPQSsl68cgiDbU2g" target="_blank">
                            <img src="{{ asset('images/yt.svg') }}" alt="yt">
                        </a>
                        <a href="https://www.instagram.com/traderindo.official/" target="_blank">
                            <img src="{{ asset('images/ig.svg') }}" alt="ig">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <script>
        $(function() {
            $(document).scroll(function() {
                var $nav = $(".header_container");
                var $menu = $(".menu_klasemen");
                var $logo = $("#logo");

                $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
                $menu.toggleClass("scrolled", $(this).scrollTop() > $nav.height());

                if ($(window).scrollTop() > $nav.height()) {
                    $logo.removeClass("logo").addClass("logo-scrolled");
                } else {
                    $logo.removeClass("logo-scrolled").addClass("logo");
                }
            });
        });

    </script>

    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=507042357408304&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
</body>

</html>
