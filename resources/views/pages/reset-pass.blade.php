<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grand Prix Series Forex Trading Indonesia</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('./images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./images/favicon/favicon-16x16.png') }}">

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    {{-- CSRF TOKEN --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>

    <div class="container">
        <div class="login_container">
            <div class="login">
                <div class="container-fluid">
                    {{-- <div class="row"> --}}
                    {{-- <div class="col-md-4 login_img"></div> --}}
                    <div class="login_right">
                        <h2>Reset Password</h2>
                        <h5>Please input a new password</h5>

                        <form class="login-form my-4" method="POST" action="{{route('postResetPass')}}">
                            @csrf
                            <div class="form-group">
                                <input type="password" class="form-control form_input" id="newPassword"
                                    name="password" value="{{ old('newPassword') }}" required
                                    placeholder="New Password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control form_input" id="confirmPassword"
                                    name="password_confirmation" value="{{ old('confirmPassword') }}" required
                                    placeholder="Confirm Password">
                            </div>
                            <input type="hidden" name="token" value="{{$token}}">
                            <button type="submit" id="btnSumbit" class="login_btn">
                                Submit
                            </button>
                        </form>

                        <div class="to_register text-center">
                            <p>
                                Your password will be used to login and confirm <br> transactions in the website
                            </p>

                        </div>

                    </div>
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </div>

    {{-- =========================================================================================================================== --}}
    <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

</body>

</html>
