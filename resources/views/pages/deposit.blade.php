@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','News')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('onpage_css')
<link href="{{URL::to('css/loading.css')}}" rel="stylesheet">
@endsection

@section('content')
  <div class="row row-sm mg-t-20">
    <div class="col-lg-6">
      <div class="section-wrapper">
        <label class="section-title">Deposit Request</label>
        <form class="" method="post" enctype="multipart/form-data">
          <div class="form-layout form-layout-4">
            <div class="row">
              <label class="col-sm-4 form-control-label">From Bank: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="bankname" value="{{$bank->bank_name}}" required>
              </div>
            </div><!-- row -->
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">From Acc. Number: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="banknumber" value="{{$bank->account_number}}" required>
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">From Acc. Name: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="bankaccount" value="{{$bank->account_name}}" required>
              </div>
            </div>

            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">To Bank : <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <select class="form-control" data-placeholder="RadioBank" name="RadioBank" id="RadioBank" onchange="bankSelect()" required>
                  <option value="">--pilih--</option>
                  <option value="BCA">BCA</option>
                  <option value="Mandiri">Mandiri</option>
                  <option value="BRI">BRI</option>
                  <option value="BNI">BNI</option>
                </select>
              </div>
            </div>

            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Amount <span class="tx-danger">*</span></label>
              <div class="col-sm-3 mg-t-10 mg-sm-t-0">
                <select class="form-control" data-placeholder="CUR" name="cur" required>
                  @if(!empty($converters))
                    @foreach($converters as $n=>$acc)
                    <option value="{{$acc->id}}">{{$acc->cur_two}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="col-sm-5 mg-t-10 mg-sm-t-0">
                <input type="number" class="form-control" name="amount" min="10000" value="" required>
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">To Account <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <select class="form-control" data-placeholder="metatrader" name="metatrader" required>
                  <option value="">--pilih--</option>
                  @if(!empty($mt4s))
                  @foreach($mt4s as $n=>$acc)
                    @if($acc->status == 'approved')
                      <option value="{{$acc->login}}">{{$acc->login}}</option>
                    @endif
                  @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Screenshot </label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="file" class="custom-file-input" id="customFile2" name="deposit_proof" >
                <label class="custom-file-label custom-file-label-primary" for="customFile2">upload here...</label>
              </div>

            </div><!-- custom-file -->


            @csrf
            <div class="form-layout-footer mg-t-30">
              <button class="btn btn-primary btn-block">Submit</button>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
        </form>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="section-wrapper">
        <label class="section-title">Beneficiary</label>

        <div class="form-layout form-layout-4">
          <div class="row">

            <label class="col-sm-4 form-control-label">To Bank: <span class="tx-danger">*</span></label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" id="bankname" name="bankname" value="" readonly>
            </div>
          </div><!-- row -->
          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label">To Acc. Number: <span class="tx-danger">*</span></label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" id="banknumber" name="banknumber" value="" readonly>
            </div>
          </div>
          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label">To Acc. Name: <span class="tx-danger">*</span></label>
            <div class="col-sm-8 mg-t-10 mg-sm-t-0">
              <input type="text" class="form-control" id="bankaccount" name="bankaccount" value="" readonly>
            </div>
          </div>
        </div>
        <label class="section-title">Pending Deposit Status</label>
        <div class="table-responsive mg-t-20">
          <table class="table table-bordered">
            <thead class="thead-colored bg-warning">
              <tr>
                <th class="wd-30p">Date time</th>
                <th class="wd-30p">To MT4</th>
                <th class="wd-15p">Amount</th>
                <th class="wd-25p">status</th>
              </tr>
            </thead>
            <tbody>

              @if(!empty($pendings))
                @foreach($pendings as $n=>$acc)

                <tr>
                  <td class="text-center">{{date('d-m-y H:i',strtotime($acc->created_at.'+7hours'))}}</td>
                  <td class="text-center">{{$acc->metatrader}}</td>
                  <td class="text-center">{{$acc->approved_amount > 1000 ? number_format($acc->approved_amount,'0',',','.') : number_format($acc->approved_amount,'2',',','.')}}</td>
                  <td class="text-warning text-center"><i class="icon ion-ios-clock-outline"></i> {{$acc->status}}</td>
                </tr>
                @endforeach
              @endif

            </tbody>
          </table>
        </div><!-- table-responsive -->
      </div>
    </div>
  </div>
  <div class="row row-sm mg-t-20">
    <div class="col-lg-12">
      <div class="section-wrapper">
        <label class="section-title">Deposit History</label>
        <div class="table-responsive mg-t-20">
          <table class="table table-bordered">
            <thead class="thead-colored bg-warning">
              <tr>
                <th class="wd-15p">Date time</th>
                <th class="wd-20p">From Bank</th>
                <th class="wd-20p">To MT4</th>
                <th class="wd-10p">Amount</th>
                <th class="wd-20p">Updated at</th>
                <th class="wd-15p">status</th>
              </tr>
            </thead>
            <tbody>
              @if(!empty($depos))
                @foreach($depos as $n=>$acc)
                @switch($acc->status)
                  @case('rejected')
                    <?php $warna = 'danger'; $icon = 'ion-android-close'?>
                  @break

                  @case('approved')
                    <?php $warna = 'success'; $icon = 'ion-android-checkmark-circle'?>
                  @break

                  @default
                    <?php $warna = 'warning';  $icon = 'ion-ios-clock-outline'?>
                @endswitch

                @if (Session::get('user.parent')=='2094' && $acc->status == 'approved')
                  <script>
                  fbq('track', 'Deposit');
                  </script>
                @endif
                <tr>
                  <td class="text-center">{{date('d-m-y H:i',strtotime($acc->created_at.'+7hours'))}}</td>
                  <td class="text-center">{{$acc->username}}</td>
                  <td class="text-center">{{$acc->metatrader}}</td>
                  <td class="text-center">{{$acc->approved_amount > 1000 ? $acc->approved_amount : number_format($acc->approved_amount,'2',',','.')}}</td>
                  <td class="text-center">{{date('d-m-y H:i',strtotime($acc->updated_at.'+7hours'))}}</td>
                  <td class="text-{{$warna}} text-center"><i class="icon {{$icon}}"></i> {{$acc->status}}</td>
                </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div><!-- table-responsive -->
      </div>
    </div>
  </div>
  <div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="content" id="loading">
                <div class="spinner">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                    <circle cx="8" cy="8" r="7" stroke-width="2" />
                    </svg>
                </div>
            </div>
            <div class="loading-text-wrapper">
                <h5 class="loading-text" style="color: white">Loading...</h5>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDepositSuccess" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-text">
                <h2>Anda telah berhasil melakukan deposit</h2>
                <button data-dismiss="modal" class="btn btn-primary confirm-button">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalverificationfail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-pendaftaran-self">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-text">
                <h2>Terjadi kesalahan dalam pengiriman data</h2>
                <h4>Silahkan coba kembali</h4>
                <button data-dismiss="modal" class="btn btn-danger confirm-button">OK</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection

@section('onpage_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type=file]').on('change', function(e) {
            var fileName =  e.target.files[0].name
            $("label[for='customFile2']").text(fileName)
            console.log(fileName)
            
        })
       $("form").on('submit', function(e) {
        //   console.log('tes')
          e.preventDefault();
          $("#modalLoading").modal('toggle');
          var formData = new FormData(this);
          $.ajax({
            url: "/grandprix/deposit/request",
            type: 'POST',
            data: formData,
            success: function (data) {
                    $("#modalLoading").modal('toggle');
                if (data == 'failed'){
                    $("#modalverificationfail").modal('toggle');
                    
                }else{
                    $("#modalDepositSuccess").modal('toggle');
                    
                }
                // console.log(data)
            },
            cache: false,
            contentType: false,
            processData: false
            });
       }) 
    })
</script>
  <script type="text/javascript">

  function bankSelect(){
    var radioBank = document.getElementById("RadioBank").value;
    console.log(radioBank);
    if( radioBank == 'BRI'){
      document.getElementById("bankname").value  = 'BRI';
      document.getElementById("banknumber").value  = 'Abdul Gani Romeon';
      document.getElementById("bankaccount").value  = '325601023241530';
    }else if( radioBank == 'Mandiri'){
      document.getElementById("bankname").value  = 'Mandiri';
      document.getElementById("banknumber").value  = 'Abdul Gani Romeon';
      document.getElementById("bankaccount").value  = '1290011457955';
    }else if( radioBank == 'BCA'){
      document.getElementById("bankname").value  = 'BCA';
      document.getElementById("banknumber").value  = 'Abdul Gani Romeon';
      document.getElementById("bankaccount").value  = '5055206969';
    }else if( radioBank == 'BNI'){
      document.getElementById("bankname").value  = 'BNI';
      document.getElementById("banknumber").value  = 'Abdul Gani Romeon';
      document.getElementById("bankaccount").value  = '0855888694';
    }
  }

  $(window).on('load',function(){
      $('#welcomeModal').modal('show');
  });
  </script>
@endsection
