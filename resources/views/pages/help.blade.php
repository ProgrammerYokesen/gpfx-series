@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Contact Us')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','Contact Us')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('onpage_css')

@endsection

@section('content')
  <div class="section-wrapper">
    <label class="section-title">Contact Us</label>

    <h3 class="mt-3">We are here to help you</h3>
    <p>Our priorities is to help you experiencing the best of our product and services. <br> Should you have any questions, facing problems during your journey in GPFX Series or in case you need to get any information about us, feel free to reach us through:</p>

    <p>
      Email: 
      <a href="mailto:contact@gpfxseries.com">contact@gpfxseries.com</a>
    </p>
  </div>

@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection