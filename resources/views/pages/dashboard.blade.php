@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','News')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('onpage_css')

@endsection

@section('content')
  <div class="row row-sm mg-t-20">
    <div class="col-lg-12">
      <div class="section-wrapper">
        <div class="row">
          <div class="col-md-8">
            <label class="section-title">Selamat datang, {{Session::get('user.name')}}</label>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row row-sm mg-t-20">
    <div class="col-lg-6">
        <div class="section-wrapper">
          <div class="row">
            <div class="col-md-12">
              <label class="section-title">Want to earn more money?</label>
              <label class="section-title">Your Affiliate referal code </label>
              <div class="input-group">
                <span class="input-group-addon"></span>
                <input type="text" class="form-control" value="https://gpfxseries.com/?ref={{Session::get('user.id')}}&so=linkib&campaign=0" id="copyIB" onclick="copyClipboard()" readonly><button class="btn btn-success" onclick="copyClipboard()">Copy text</button>
              </div>
            </div>
          </div>
        </div>
    </div>
    
    <!--Terms&Condition-->
    <div class="col-lg-6">
        <div class="section-wrapper">
          <div class="row">
            <div class="col-md-12">
              <label class="section-title">Terms & Condition</label>
              <label class="section-title">Klik <a href="{{asset('./files/Terms&Condition.pdf')}}" target="_blank">disini</a> untuk melihat Terms & Condition</label>
              <!--<div class="input-group">-->
              <!--  <span class="input-group-addon"></span>-->
              <!--  <input type="text" class="form-control" value="https://gpfxseries.com/?ref={{Session::get('user.id')}}&so=linkib&campaign=0" id="copyIB" onclick="copyClipboard()" readonly><button class="btn btn-success" onclick="copyClipboard()">Copy text</button>-->
              <!--</div>-->
            </div>
          </div>
        </div>
    </div>
  </div>

@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection
