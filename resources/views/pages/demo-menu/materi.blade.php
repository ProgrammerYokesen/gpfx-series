@extends('template.master')
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','Pengumuman')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="lab-title">materi</div>
    <div class="announcement-list">
        @foreach ($announcements as $item)
            <a type="button" data-toggle="modal" data-target="#announcementModal{{ $item->id }}">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="https://gpfxseries.com/{{ $item->photo }}" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">{{ $item->title }}</p>
                        <p class="card-text">Published in : {{ $item->created_at }}</p>
                    </div>
                </div>
            </a>

            <div class="modal fade" id="announcementModal{{ $item->id }}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{ $item->title }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <img class="card-img-modal"
                                    src="https://gpfxseries.com/{{ $item->photo }}"
                                    alt="Card image cap">
                            </div>
                            <div>
                                <p>{{ $item->description }}</p>
                                @if($item->link)
                                <a href="/{{ $item->link }}" download="{{$item->link}}" class="mt-2 btn btn-primary">Download Attachment</a>
                                @endif
                                <p class="card-text mt-4">Published in : {{ $item->created_at }}</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @endforeach
    </div>


@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection