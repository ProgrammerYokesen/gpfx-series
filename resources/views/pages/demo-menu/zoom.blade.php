@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Zoom Meeting')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','Zoom Meeting')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('onpage_css')

@endsection

@section('content')
@foreach($zooms as $zoom)
  <div class="section-wrapper">
    <label class="section-title">Zoom Meeting</label>

    <h5 class="mt-3 mb-2">{{ date('d M Y H:i', strtotime($zoom->date)) }}</h5>
    <h3 class="mb-3">{{ $zoom->title }}</h3>
    
    <a href="{{ $zoom->link }}" target="_blank">
        <button class="btn btn-primary btn-lg mg-b-10">Join Zoom Meeting</button>
    </a>
  </div>
@endforeach

@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection