<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Grand Prix Series Forex Trading Indonesia</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('./images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('./images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('./images/favicon/favicon-16x16.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/error.css') }}" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />

</head>

<body>
    <!-- HEADER -->
    <div class="header_error_container">
        <div class="container">
            <a href="{{ route('homePage') }}">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="header_logo">
            </a>
            <div class="menu_error">
                <a href="#" class="menu_klasemen_error">KLASEMEN</a>
                <a href="{{ route('masuk') }}">
                    <button>MASUK</button>
                </a>
            </div>
        </div>
    </div>

    @yield('homeError')


    <!-- FOOTER -->
    {{-- <div class="footer">
        <div class="container">
            <div class="footer_container">
                <div>
                    <h2>Ikuti sosial media Traderindo</h2>
                    <div class="footer_socmed">
                        <a href="https://www.facebook.com/groups/komunitastraderindo" target="_blank">
                            <img src="{{ asset('images/fb.svg') }}" alt="fb">
                        </a>
                        <a href="https://www.youtube.com/channel/UCTVy3OUjPQSsl68cgiDbU2g" target="_blank">
                            <img src="{{ asset('images/yt.svg') }}" alt="yt">
                        </a>
                        <a href="https://www.instagram.com/traderindo.official/" target="_blank">
                            <img src="{{ asset('images/ig.svg') }}" alt="ig">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- END FOOTER -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>

    <!-- End Facebook Pixel Code -->
</body>

</html>
