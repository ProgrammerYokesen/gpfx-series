@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','News')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('onpage_css')

@endsection

@section('content')
  <div class="section-wrapper">
    <label class="section-title">Akun Trading Kamu</label>
    <div class="table-responsive mg-t-20">
      <table class="table table-bordered">
        <thead class="thead-colored bg-warning">
          <tr>
            <th class="wd-5p" class="text-center">No</th>
            <th class="wd-15p" class="text-center">Type</th>
            <th class="wd-20p" class="text-center">Account Number</th>
            <th class="wd-20p" class="text-center">Password</th>
            <th class="wd-20p" class="text-center">status</th>
          </tr>
        </thead>
        <tbody>

          @if(!empty($lists))
            @php
                $waiting = "";
            @endphp
            @foreach($lists as $n=>$acc)
                @if($acc->status == "waiting")
                   @php 
                    $waiting = "true";
                   @endphp
                @endif
            @switch($acc->status)
              @case('rejected')
                <?php $warna = 'danger'; $icon = 'ion-android-close'?>
              @break

              @case('approved')
                <?php $warna = 'success'; $icon = 'ion-android-checkmark-circle'?>
              @break

              @default
                <?php $warna = 'warning';  $icon = 'ion-ios-clock-outline'?>
            @endswitch
              @if($acc->status != "rejected")

                <tr>
                  <th scope="row">{{$n+1}}</th>
                  <td>{{$acc->type}}</td>
                  <td class="text-center">{{$acc->login}}</td>
                  <td class="text-center">******** <a href="{{route('accountReset',$acc->id)}}"><sup>{{$acc->pswd_meta !='' ? 'reset' : '' }}</sup></a></td>
                  <td class="text-center text-{{$warna}}"><i class="icon {{$icon}}"></i> {{$acc->status}}</td>
                </tr>
              @endif
            @endforeach
            @if($waiting == "true")
                <tr>
                    <td colspan="5"><a href="#" style="text-decoration:none; color:black; cursor:default;">Request Akun Baru (klik di sini)</a></td>
                </tr>
            @else
                <tr>
                    <td colspan="5"><a href="{{route('requestAccount')}}">Request Akun Baru (klik di sini)</a></td>
                </tr>
            @endif
         @else
          <tr>
                <td colspan="5"><a href="{{route('requestAccount')}}">Request Akun Baru (klik di sini)</a></td>
          </tr>
          @endif
      </table>

    </div><!-- table-responsive -->
  </div>


@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection
