@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','News')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
@endsection

@section('onpage_css')

@endsection

@section('content')
<div class="row row-sm mg-t-20">
  <div class="col-lg-6">
    <div class="section-wrapper">
      <label class="section-title">Withdrawal Request</label>
      <form class="" action="{{route('withdrawCreate')}}" method="post" enctype="multipart/form-data">
      <div class="row mg-t-20">
        <label class="col-sm-4 form-control-label">From Account <span class="tx-danger">*</span></label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <select class="form-control select2" data-placeholder="metatrader" name="metatrader" required>
            <option value="">--pilih--</option>
            @if(!empty($mt4s))
            @foreach($mt4s as $n=>$acc)
              @if($acc->status == 'approved')
                <option value="{{$acc->login}}">{{$acc->login}}</option>
              @endif
            @endforeach
            @endif
          </select>
        </div>
      </div>
      <div class="row mg-t-20">
        <label class="col-sm-4 form-control-label">To Bank: <span class="tx-danger">*</span></label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" name="bankname" value="{{$bank->bank_name}}" readonly>
        </div>
      </div><!-- row -->
      <div class="row mg-t-20">
        <label class="col-sm-4 form-control-label">To Acc. Number: <span class="tx-danger">*</span></label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" name="banknumber" value="{{$bank->account_number}}" readonly>
        </div>
      </div>
      <div class="row mg-t-20">
        <label class="col-sm-4 form-control-label">To Acc. Name: <span class="tx-danger">*</span></label>
        <div class="col-sm-8 mg-t-10 mg-sm-t-0">
          <input type="text" class="form-control" name="bankaccount" value="{{$bank->account_name}}" readonly>
        </div>
      </div>


        <div class="form-layout form-layout-4">

          <div class="row mg-t-20">
            <label class="col-sm-4 form-control-label">Amount <span class="tx-danger">*</span></label>
            <div class="col-sm-3 mg-t-10 mg-sm-t-0">
              $
            </div>
            <div class="col-sm-5 mg-t-10 mg-sm-t-0">
              <input type="number" class="form-control" name="amount" value="" min="1" required>
            </div>
          </div>
          @csrf
          <div class="form-layout-footer mg-t-30">
            <button class="btn btn-primary btn-block">Submit</button>
          </div><!-- form-layout-footer -->
        </div><!-- form-layout -->
      </form>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="section-wrapper">
      <label class="section-title">Last 5 Withdrawal Status</label>
      <div class="table-responsive mg-t-20">
        <table class="table table-bordered">
          <thead class="thead-colored bg-warning">
            <tr>
              <th class="wd-30p">Date time</th>
              <th class="wd-15p">From</th>
              <th class="wd-15p">Amount</th>
              <th class="wd-25p">status</th>
            </tr>
          </thead>
          <tbody>

            @if(!empty($pendings))
              @foreach($pendings as $n=>$acc)

              <tr>
                <td class="text-center">{{date('d-m-y H:i',strtotime($acc->created_at.'+7hours'))}}</td>
                <td class="text-center">{{$acc->metatrader}}</td>
                <td class="text-center">{{$acc->amount > 1000 ? number_format($acc->amount,'0',',','.') : number_format($acc->amount,'2',',','.')}}</td>
                <td class="text-warning text-center"><i class="icon ion-ios-clock-outline"></i> {{$acc->status}}</td>
              </tr>
              @endforeach
            @endif

          </tbody>
        </table>
      </div><!-- table-responsive -->
    </div>
  </div>
</div>
<div class="row row-sm mg-t-20">
  <div class="col-lg-12">
    <div class="section-wrapper">
      <label class="section-title">Withdrawal History</label>
      <div class="table-responsive mg-t-20">
        <table class="table table-bordered">
          <thead class="thead-colored bg-warning">
            <tr>
              <th class="wd-20p">Create time</th>
              <th class="wd-10p">From Account </th>
              <th class="wd-10p">Amount </th>
              <th class="wd-20p">Process Time</th>
              <th class="wd-20p">status</th>
              <th class="wd-20p">Detail</th>
            </tr>
          </thead>
          <tbody>
            @if(!empty($histories))
              @foreach($histories as $n=>$acc)
              @switch($acc->status)
                @case('rejected')
                  <?php $warna = 'danger'; $icon = 'ion-android-close'?>
                @break

                @case('approved')
                  <?php $warna = 'success'; $icon = 'ion-android-checkmark-circle'?>
                @break

                @default
                  <?php $warna = 'warning';  $icon = 'ion-ios-clock-outline'?>
              @endswitch
              @if (Session::get('user.parent')=='2094' && $acc->status == 'approved')
                <script>
                fbq('track', 'Withdrawal');
                </script>
              @endif
              <tr>
                <td class="text-center">{{date('d-m-y H:i',strtotime($acc->created_at.'+7hours'))}}</td>
                <td class="text-center">{{$acc->metatrader}}</td>
                <td class="text-center">{{$acc->amount > 1000 ? $acc->amount : number_format($acc->amount,'2',',','.')}}</td>
                <td class="text-center">{{date('d-m-y H:i',strtotime($acc->updated_at.'+7hours'))}}</td>
                <td class="text-{{$warna}} text-center"><i class="icon {{$icon}}"></i> {{$acc->status}}</td>
                <td><a href="#modal{{$acc->id}}" class="modal-effect btn btn-primary btn-block" data-toggle="modal" data-effect="effect-scale">detail</a></td>
              </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div><!-- table-responsive -->
    </div>
  </div>
</div>


@foreach($histories as $n => $acc)
@switch($acc->status)
  @case('rejected')
    <?php $warna = 'danger'; $icon = 'ion-android-close'?>
  @break

  @case('approved')
    <?php $warna = 'success'; $icon = 'ion-android-checkmark-circle'?>
  @break

  @default
    <?php $warna = 'warning';  $icon = 'ion-ios-clock-outline'?>
@endswitch
<div id="modal{{$acc->id}}" class="modal fade">
  <div class="modal-dialog modal-lg" role="document" style="width:80%">
    <div class="modal-content bd-0 tx-size-sm">
      <div class="modal-header pd-y-20 pd-x-25">
        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Detail Withdrawal</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pd-25">
        <h5 class="lh-3 mg-b-20 tx-inverse"></h5>
        <div class="row">
          <div class="col-md-6">
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Create Time</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                {{$acc->created_at}}
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">To Bank</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                {{$acc->bank_name}}
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Acc Name</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                {{$acc->account_name}}
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Acc Number</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                {{$acc->account_number}}
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Process Time</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                {{$acc->updated_at}}
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Amount</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                {{$acc->amount}} $
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Status</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0 text-{{$warna}}">
                <i class="icon {{$icon}}"></i> {{$acc->status}}
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Note</label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0 text-{{$warna}}">
                {{$acc->status == "rejected" ? $acc->reason : ''}}
              </div>
            </div>
          </div>
        </div>


      </div>
      <div class="modal-footer">
        This data is verified by accounting system
      </div>
    </div>
  </div><!-- modal-dialog -->
</div><!-- modal -->
@endforeach

@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
@endsection
