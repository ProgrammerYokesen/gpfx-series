@extends('template.master')

@section('title', 'Dashboard'.' '.env('APP_NAME'))
@section('description','Member Dashboard'.' '.env('APP_NAME'))
@section('breadcrumb-first','Member')
@section('breadcrumb-second','Dashboard')
@section('breadcrumb-third','News')
@section('class-body','')

@section('vendor_css')
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
  
@endsection

@section('onpage_css')
  <link href="{{URL::to('css/loading.css')}}" rel="stylesheet">
  {{-- select2 --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
        integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
        crossorigin="anonymous" />

@endsection

@section('content')
  <div class="row row-sm mg-t-20">
    <div class="col-lg-12">
      <div class="section-wrapper">
        <div class="row">
          <div class="col-md-8">
            <label class="section-title">PENDAFTARAN GRAND PRIX FOREX INDONESIA 2021</label>
            @if ($profile->status != 'verified' && $profile->uploadid != null)
                <div id="status-waiting">
                    <i class="fa fa-info-circle" style="color:#FFDD77;">Your account under verification</i>
                </div>
            @endif
                <div id="status">
                    
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row row-sm mg-t-20">
    <div class="col-md-6">
      <div class="section-wrapper">
        <label class="section-title">Profile Data</label>
        <p class="mg-b-20 mg-sm-b-40">Please complete your profile.</p>
        <form method="post" action="{{route('postDaftarGrandPrix')}}"  enctype="multipart/form-data" id="pendaftaranForm">
          <div class="form-layout form-layout-4">
            <div class="row">
              <label class="col-sm-4 form-control-label">Nama sesuai KTP: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="fullname" value="{{$profile->fullname}}" {{$profile->status == 'verified' ? 'readonly' : ''}} required>
                <div id="fullname">

                </div>
              </div>
            </div><!-- row -->
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Email: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="email" value="{{$profile->email}}" readonly>
                <div id="email">

                </div>
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Whatsapp: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">+62</span>
                  </div>
                  <input type="text" class="form-control" name="phone" value="{{$profile->phone}}" {{$profile->status == 'verified' ? 'readonly' : ''}} placeholder="tanpa angka 0"  required>
                  <div id="phone">

                  </div> 
                </div><!-- input-group -->
              </div>
            </div>
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Date of Birth <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                    </div>
                  </div>
                  <input type="date" name="dob" class="form-control" value="{{$profile->dob}}" {{$profile->status == 'verified' ? 'readonly' : ''}}  required>
                  
                </div><!-- input-group -->
                <div id="dob">

                 </div>
              </div>
            </div>
            <input type="hidden" name="id" value="{{$profile->id}}">
          </div><!-- form-layout -->
        </div><!-- section-wrapper -->
        <br>
        <div class="section-wrapper">
          <label class="section-title">Bank Account</label>
          <p class="mg-b-20 mg-sm-b-40">Please input your bank account information.</p>
          <div class="form-layout form-layout-4">
            <div class="row">
              <label class="col-sm-4 form-control-label">Bank Name: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="bankname" value="{{$bank->bank_name}}" {{$profile->status == 'verified' ? 'readonly' : ''}}  required>
                <div id="bankname">

                </div>
              </div>
            </div><!-- row -->
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Account Number: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="banknumber" value="{{$bank->account_number}}" {{$profile->status == 'verified' ? 'readonly' : ''}}  required>
                <div id="banknumber">

                </div>
              </div>
            </div><!-- row -->
            <div class="row mg-t-20">
              <label class="col-sm-4 form-control-label">Account Name: <span class="tx-danger">*</span></label>
              <div class="col-sm-8 mg-t-10 mg-sm-t-0">
                <input type="text" class="form-control" name="bankaccount" value="{{$bank->account_name}}" {{$profile->status == 'verified' ? 'readonly' : ''}} required>
                <div id="bankaccount">

                </div>
              </div>
            </div><!-- row -->
            <input type="hidden" name="bank_id" value="{{$bank->id}}">
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="section-wrapper">
          <label class="section-title">Documents</label>
          @if($profile->status != 'verified')
            <p class="mg-b-20 mg-sm-b-40">Please upload your ID.</p>
            <div class="row mg-t-20">
              <div class="col-lg-6 mg-t-40 mg-lg-t-0">
                @if($profile->uploadid)
                  <img src="/storage/{{str_replace('https://fed.financiafx.com/','',$profile->uploadid)}}" class="img-fluid img-thumbnail" alt="">
                @else
                  <img src="/storage/default.png" class="img-fluid img-thumbnail" alt="" id="uploadIDPreview">
                @endif
              </div><!-- col -->
              <div class="col-lg-6 mg-t-40 mg-lg-t-0">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile2" name="uploadid" onchange="readIDPreview(this);" required>
                  <div id="uploadid">

                  </div>
                  <label class="custom-file-label custom-file-label-primary" for="customFile2">Choose file</label>
                </div><!-- custom-file -->
              </div><!-- col -->

            </div>
            <hr>
            <p class="mg-b-20 mg-sm-b-40">Please upload your Bank Account Cover Book</p>
            <div class="row mg-t-20">
              <div class="col-lg-6 mg-t-40 mg-lg-t-0">
                @if($profile->uploadaddress)
                  <img src="/storage/{{str_replace('https://fed.financiafx.com/','',$profile->uploadaddress)}}" class="img-fluid img-thumbnail" alt="">
                @else
                  <img src="/storage/default.png" class="img-fluid img-thumbnail" alt="" id="uploadCoverPreview">
                @endif
              </div><!-- col -->


              <div class="col-lg-6 mg-t-40 mg-lg-t-0">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFile1" name="uploadaddress" onchange="readCoverPreview(this);" required>
                  <div id="uploadaddress">

                  </div>
                  <label class="custom-file-label custom-file-label-primary" for="customFile1">Choose file</label>
                </div><!-- custom-file -->
              </div><!-- col -->

            </div>
        @else
          <div class="row mg-t-20">
            <div class="col-lg-12 mg-t-40 mg-lg-t-0">
              <img src="/storage/{{str_replace('https://fed.financiafx.com/','',$profile->uploadid)}}" class="img-fluid img-thumbnail" alt="">
            </div><!-- col -->
          </div>
          <div class="row mg-t-20">
            <div class="col-lg-12 mg-t-40 mg-lg-t-0">
              <img src="/storage/{{str_replace('https://fed.financiafx.com/','',$profile->uploadaddress)}}" class="img-fluid img-thumbnail" alt="">
            </div><!-- col -->
          </div>
        @endif
      </div>
      <br>
      @if($profile->status != 'verified')
        <div class="section-wrapper">
          <label class="section-title">Persyaratan</label>
          <input type="hidden" name="id" value="{{$profile->id}}">
          <div class="row row-xs mg-b-10">
            <div class="col-sm">
              <label class="ckbox">
                <input type="checkbox"  name="daftar-traderindo" value="1" required><span><small>Dengan mengikuti Grand Prix Forex 2021 ini Saya sekaligus mendaftarkan diri ke website https://traderindo.com dan hasil trading akan dipublish di Traderindo. </small> </span>
                <div id="daftar-traderindo">

                </div>
              </label>
            </div>
          </div>
          <div class="row row-xs mg-b-10">
            <div class="col-sm">
              <label class="ckbox">
                <input type="checkbox" id="emailPromosiInput" name="daftar-traderindo" value="1" ><span><small>Saya bersedia menerima email promosi dari GrandPrix Forex 2021 dan Traderindo. </small> </span>
              </label>
            </div>
          </div>
          @csrf
          <div class="form-layout-footer mg-t-30">
            <a class="btn btn-primary btn-block" data-toggle="modal" data-target="#modalConfirm" style="color:white;">Submit</a>
            <!--<button class="btn btn-primary btn-block" >Submit</button>-->
          </div><!-- form-layout-footer -->
        </form>
        </div>
      @else
        <div class="section-wrapper">
          <label class="section-title">Selamat Kamu sudah terdaftar sebagai peserta Grand Prix Forex 2021</label>
          <p>Selanjutnya Kamu butuh akun trading untuk mengikuti Grand Prix ini.</p>
          <p>Klik tombol berikut untuk mendapatkan Akun Trading</p>
          <a href="{{route('requestAccount')}}" class="btn btn-primary btn-lg btn-block">Dapatkan Akun Trading</a>
        </form>
        </div>
      @endif

    </div><!-- col-6 -->
  </div>
  <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalLoading">modal loading</button>-->
<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-pendaftaran-self">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-text">
                <h3>Apakah anda sudah yakin dengan informasi yang akan anda kirim?</h3>
                <h5>Anda tidak bisa mengganti data ini setelah terkitim.</h5>
                <button id="pendaftaranGrandprix" type="submit" form="pendaftaranForm" data-dismiss="modal" class="btn btn-primary confirm-button" style="margin-top:25px">Confirm</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="background: transparent">
    <!--    <div class="modal-content">-->
            <div class="content" id="loading">
                <div class="spinner">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                    <circle cx="8" cy="8" r="7" stroke-width="2" />
                    </svg>
                </div>
            </div>
            <div class="loading-text-wrapper">
                <h5 class="loading-text" style="color: white">Loading...</h5>
            </div>
    <!--    </div>-->
    </div>
</div>
<div class="modal fade" id="modalverification" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-pendaftaran-self">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-text">
                <h2>Akun anda sedang dalam proses verifikasi</h2>
                <button data-dismiss="modal" class="btn btn-primary confirm-button">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalverificationfail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-pendaftaran-self">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-text">
                <h2>Terjadi kesalahan dalam pengiriman data</h2>
                <h4>Silahkan coba kembali</h4>
                <button data-dismiss="modal" class="btn btn-danger confirm-button">OK</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('vendor_js')
  <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
  <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
  <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
  <script src="{{url('/')}}/lib/jquery.cookie/js/jquery.cookie.js"></script>
  <script src="{{url('/')}}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
  <script src="{{url('/')}}/lib/jt.timepicker/js/jquery.timepicker.js"></script>
  <script src="{{url('/')}}/lib/spectrum/js/spectrum.js"></script>
  <script src="{{url('/')}}/lib/jquery.maskedinput/js/jquery.maskedinput.js"></script>
  <script src="{{url('/')}}/lib/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
        integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
        crossorigin="anonymous"></script>
@endsection

@section('onpage_js')
    
  <script>
    $(function(){
      'use strict'
      // Input Masks
      $('#dateMask').mask('99/99/9999');

      $('.form-layout .form-control').on('focusin', function(){
        $(this).closest('.form-group').addClass('form-group-active');
      });

      $('.form-layout .form-control').on('focusout', function(){
        $(this).closest('.form-group').removeClass('form-group-active');
      });

      // Select2
      $('.select2').select2({
        minimumResultsForSearch: Infinity
      });

      $('#select2-a, #select2-b').select2({
        minimumResultsForSearch: Infinity
      });

      $('#select2-a').on('select2:opening', function (e) {
        $(this).closest('.form-group').addClass('form-group-active');
      });

      $('#select2-a').on('select2:closing', function (e) {
        $(this).closest('.form-group').removeClass('form-group-active');
      });


    });

    function copyClipboard() {
      /* Get the text field */
      var copyText = document.getElementById("copyIB");

      /* Select the text field */
      copyText.select();

      /* Copy the text inside the text field */
      document.execCommand("copy");

      /* Alert the copied text */
      alert("Copied the text: " + copyText.value);
    }


    $(window).on('load',function(){
        $('#welcomeModal').modal('show');
    });

    function readIDPreview(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        $("label[for='customFile2']").text(input.files[0].name);
        reader.onload = function (e) {
          $('#uploadIDPreview')
          .attr('src', e.target.result)
          .width(200)
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readCoverPreview(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        $("label[for='customFile1']").text(input.files[0].name);
        reader.onload = function (e) {
          $('#uploadCoverPreview')
          .attr('src', e.target.result)
          .width(200)
        };

        reader.readAsDataURL(input.files[0]);
      }
    }

  </script>
@if($profile->status != 'verified')

  <script>
        $(document).ready(function() {
            $("#pendaftaranForm").on('submit', function(e) {
                e.preventDefault();
                var fullName= $('#fullNameInput').val();
                // console.log(fullName)
                var formData = new FormData(this);
                $("#modalLoading").modal('toggle');
                $.ajax({
                    url: '/grandprix/action/daftar',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                            $("#modalLoading").modal('toggle');
                            console.log(data)
                        if(data == 'failed'){
                            $("#modalverificationfail").modal('toggle');
                            console.log('failed')
                                console.log(data)
                        }if(data[0] == 'failed'){
                            for (var datum in data[1]){
                                $('#'+datum).empty();
                                $('#'+datum).append(
                                    `<p class="text-danger">This field is required</p>`
                                )
                                console.log('failed[0]')
                                console.log(data)
                            }
                        }else{
                                console.log(data)
                            console.log('success')
                            $("#modalverification").modal('toggle');
                            $('#status').empty();
                            $('#status').append(
                                `<i class="fa fa-info-circle" style="color:#FFDD77;">Your account under verification</i>`)
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                    });
            })
            
            $('#pendaftaranGrandprix').on('click', function() {
                $('#pendaftaranForm').submit();
            })
        }) 

    </script>
@endif
@endsection
