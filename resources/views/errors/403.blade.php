@extends('../pages.error-master')

@section('homeError')
    <div class="error_container">
        <div class="container text-center">

            <div class="error_title">
                <h3>ERROR</h3>
                <h1>403</h1>
                <h3>Akses Dilarang</h3>
            </div>

            <img src="{{ asset('images/403.svg') }}" alt="" class="error_img img-fluid">

            <div class="error_text">
                <p>Akses ke Laman ini ditolak. Jangan khawatir, kami akan membantu anda mencari</p>
                <p>apa yang anda butuhkan. Halaman utama adalah tempat memulai yang baik</p>
            </div>
            <div class="redirect-wrapper">
                <a href="{{ route('homePage') }}" class="btn redirect_btn">KEMBALI KE HALAMAN UTAMA</a>
            </div>
        </div>
    </div>
@endsection
