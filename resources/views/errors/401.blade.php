@extends('../pages.error-master')

@section('homeError')
    <div class="error_container">
        <div class="container text-center">

            <div class="error_title">
                <h3>ERROR</h3>
                <h1>401</h1>
                <h3>Unauthorized</h3>
                <h5>Maaf, kamu tidak memiliki akses ke laman ini </h5>
            </div>

            <img src="{{ asset('images/401.svg') }}" alt="" class="error_img img-fluid">

            <div class="error_text">
                <p>Jangan khawatir, coba segarkan ulang laman ini lalu </p>
                <p>masuk kembali dengan detail Login yang benar.</p>
            </div>
            <div class="redirect-wrapper">
                <a href="{{ route('homePage') }}" class="btn redirect_btn">KEMBALI KE HALAMAN UTAMA</a>
            </div>
        </div>
    </div>
@endsection
