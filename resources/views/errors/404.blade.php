@extends('../pages.error-master')

@section('homeError')
    <div class="error_container">
        <div class="container text-center">

            <div class="error_title">
                <h2>ERROR</h2>
                <h1>404</h1> {{-- Ganti Kode Error --}}
                <h5>Halaman tidak dapat ditemukan.</h5>
            </div>

            <img src="{{ asset('images/404.svg') }}" alt="" class="error_img img-fluid"> {{-- Ganti File Image --}}

            <div class="error_text">
                <p>Jangan khawatir, kami akan membantu anda mencari apa yang anda butuhkan.</p> {{-- Ganti Text Error Baris 1 --}}
                <p>Halaman utama adalah tempat memulai yang baik</p> {{-- Ganti Text Error Baris 2 --}}
            </div>
            <div class="redirect-wrapper">
                <a href="{{ route('homePage') }}" class="btn redirect_btn">KEMBALI KE HALAMAN UTAMA</a>
            </div>
        </div>
    </div>
@endsection
