@extends('../pages.error-master')

@section('homeError')
    <div class="error_container">
        <div class="container text-center">

            <div class="error_title">
                <h3>ERROR</h3>
                <h1>400</h1>
                <h3>Permintaan Buruk</h3>
            </div>

            <img src="{{ asset('images/400.svg') }}" alt="" class="error_img img-fluid">

            <div class="error_text">
                <p>Jangan khawatir, kami akan membantu anda mencari apa yang anda butuhkan.</p>
                <p>Halaman utama adalah tempat memulai yang baik</p>
            </div>
            <div class="redirect-wrapper">
                <a href="{{ route('homePage') }}" class="btn redirect_btn">KEMBALI KE HALAMAN UTAMA</a>
            </div>
        </div>
    </div>
@endsection
