<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--<link rel="shortcut icon" href="{{ url('/') }}/img/asset_favicon.png">-->
    <link rel="apple-touch-icon" sizes="180x180"
        href="{{ asset('./images/favicon-dashboard/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32"
        href="{{ asset('./images/favicon-dashboard/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16"
        href="{{ asset('./images/favicon-dashboard/favicon-16x16.png') }}">

    <!-- Meta -->
    <meta name="description" content="@yield('description')">
    <meta name="author" content="gpfx">

    <title>@yield('title')</title>

    <!-- vendor css -->
    @yield('vendor_css')
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>


    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{ url('/') }}/css/slim.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/sweetalert.css">
    @yield('onpage_css')
    <link rel="stylesheet" href="{{ url('/') }}/css/loading.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/lab.css">


    
    {{-- JQUERY --}}
    <!--<script src="https://code.jquery.com/jquery-3.6.0.js"-->
    <!--    integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>-->

</head>

<body class="@yield('class-body')">
    @include('template.header')

    <div class="slim-body">

        @include('template.sidebar')


        <div class="slim-mainpanel">
            <div class="container">
                <div class="slim-pageheader">
                    <ol class="breadcrumb slim-breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">@yield('breadcrumb-second')</a></li>
                        <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb-third')</li>
                    </ol>
                    <h6 class="slim-pagetitle">@yield('breadcrumb-first')</h6>
                </div><!-- slim-pageheader -->

                @yield('content')

            </div><!-- container -->

            @include('template.footer')
        </div><!-- slim-mainpanel -->
    </div><!-- slim-body -->

    </div>

    <!--LOADING-->
    <div class="modal fade" id="modalLoading" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="background: transparent">

            <div class="content" id="loading">
                <div class="spinner">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="8" cy="8" r="7" stroke-width="2" />
                    </svg>
                </div>
            </div>
            <div class="loading-text-wrapper">
                <h5 class="loading-text" style="color: white">Loading...</h5>
            </div>

        </div>
    </div>


    @yield('vendor_js')
    <script src="{{ url('/') }}/js/ResizeSensor.js"></script>
    <script src="{{ url('/') }}/js/slim.js"></script>
    <script src="{{ url('/') }}/js/sweetalert.min.js"></script>
    @yield('onpage_js')
    @include('sweet::alert')

    <!--Ion Icons-->
    <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>

    <script type="text/javascript">

    </script>

    <script> 
        $(".move__page").click(function() {
            $("#modalLoading").modal('toggle');
        });
        // $(".sidebar-nav-link").click(function() {
        //     $("#modalLoading").modal('toggle');
        // });
    </script>

</body>

</html>
