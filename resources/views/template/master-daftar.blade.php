<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!-- Required meta tags -->

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="csrf_token" content="{{ csrf_token() }}">
  <!-- Meta -->
  <meta name="description" content="GrandPrix Forex">
  <meta name="author" content="FinanciaFx">

  <title>Grand Prix Forex</title>
  {{--
  <meta property="og:url"                content="https://ims.financiafx.id/lp/robot/newbie" />
  <meta property="og:type"               content="landing page" />
  <meta property="og:title"              content="Gajahmada Robot Trading Forex | FinanciaFx" />
  <meta property="og:description"        content="Robot Trading Forex Tanpa Perlu Belajar | FinanciaFx" />
  <meta property="og:image"              content="{{url('/')}}/ea/banner/instagram-financiafx-alt-8-16x9.jpg" />
  --}}
  <!-- Vendor css -->
  <link href="{{url('/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="{{url('/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">

  <!-- Slim CSS -->
  <link rel="stylesheet" href="{{url('/')}}/css/slim.css">
  <link rel="stylesheet" href="{{url('/')}}/css/sweetalert.css">
  <link rel="stylesheet" href="{{ url('/') }}/css/dashboard.css">

  <style media="screen">
  .gradient_right{
    color:#ffffff;
    background-color: #ff0000; /* For browsers that do not support gradients */
    background-image: linear-gradient(to top right, #0a2e7d,#061541); /* Standard syntax (must be last) */
  }

  .gradient_left{
    background-color: #c49229; /* For browsers that do not support gradients */
    background-image: linear-gradient(to top right, #c49229,#f9e655,#91660f); /* Standard syntax (must be last) */
    color:black;
  }
  </style>

  </head>
  <body>

    <div class="d-md-flex">
      <div class="signin-left">
            <div class="text-center">
                <h1>DAFTAR SEKARANG</h1>
                <h2>Dan dapatkan Akun untuk mengikuti</h2>
                <img src="{{ asset('images/logo.png') }}" alt="">
            </div>
        </div><!-- signin-right -->
      <div class="signin-right">
        <div class="signin-box signup" style="background-color:rgba(0, 0, 0, 0.0);">
          <a class="slim-logo center"  href="https://ims.financiafx.id" target="_blank"><h2 style="margin-left: 38%" ><img src="/img/logo.png" alt="" width="120"></h2></a>

          <h3 class="signin-title-primary">Daftar Sekarang Juga! </h3>
          <div id="error-box">

            @if ($errors->has('name'))
              <div class='alert alert-warning'>
                {{ $errors->first('name') }}
              </div>
            @endif

            @if ($errors->has('email'))
              <div class='alert alert-warning'>
                {{ $errors->first('email') }}
              </div>
            @endif

            @if ($errors->has('password'))
              <div class='alert alert-warning'>
                {{ $errors->first('password') }}
              </div>
            @endif
          </div>
          <form class="" action="{{route('postDaftar')}}" method="post">
            <div class="row row-xs mg-b-10">
              <div class="col-sm"><input type="text" class="form-control" name="name" placeholder="Full name" required value="{{ Session::get('old_name')  }}" id="pendaftaran"></div>
              <div class="col-sm mg-t-10 mg-sm-t-0"><input type="email" class="form-control" name="email" placeholder="email" required value="{{ Session::get('old_email')  }}"></div>
            </div><!-- row -->

            <div class="row row-xs mg-b-10">
              <div class="col-sm">
                {{--<label>One click register with your Whatsapp</label>--}}
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">+62</span>
                  </div>
                  <input type="text" class="form-control" name="wa" placeholder="Whatsapp Number" required value="{{ Session::get('old_wa')  }}">
                </div>
              </div>
              <div class="col-sm mg-t-10 mg-sm-t-0"><input type="password" class="form-control" name="password" placeholder="Password" maxlength=32 required></div>
            </div><!-- row -->
            <div class="row row-xs mg-b-10">
              <div class="col-sm">

                {!! NoCaptcha::display() !!}
                @if ($errors->has('g-recaptcha-response'))
                  <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                  </span>
                @endif
              </div>

            </div>
            <div class="row row-xs mg-b-10">
              <div class="col-sm">
                <label class="ckbox">
                  <input type="checkbox" name="disclaimer" value="1" required><span>I have read & agree to <a href="https://financiafx.id/wp-content/uploads/2020/02/Trading-Rules-FinanciaFX-BSC-VIP-VVIP-SHARIA-Trading-hours-04022020.pdf" target="_blank">Term and Conditions</a></span>
                </label>
              </div>
            </div>
            <div class="row row-xs mg-b-10">
              <div class="col-sm">
                <label class="ckbox">
                  <input type="checkbox" name="disclaimer-2" value="1" required><span><small>FinanciaFX tidak menjanjikan fixed income, keuntungan, maupun manage fund. Mohon baca disclaimer <a href="#disclaim">di bawah ini</a> </small> </span>
                </label>
              </div>
            </div>
            <input type='hidden' name="dari" value="{{!isset($_GET['utm_source']) ? Cookie::get('utm_source') : $_GET['utm_source'] }}"/>
            <input type='hidden' name="ref" value="4173"/>
            <input type='hidden' name="uuid" value="{{ !isset($_GET['utm_content']) ? Cookie::get('utm_content') : $_GET['utm_content'] }}"/>
            <input type='hidden' name="so" value="{{ !isset($_GET['utm_medium']) ? Cookie::get('utm_medium') : $_GET['utm_medium'] }}"/>
            <input type='hidden' name="campaign" value="{{ !isset($_GET['utm_campaign']) ? Cookie::get('utm_campaign') : $_GET['utm_campaign'] }}"/>
            @csrf
            <button class="btn btn-primary btn-block btn-signin">Register</button>
          </form>

          <p>
            <a href="{{url('masuk')}}" class="btn btn-secondary pd-x-25 tx-right mg-b-20">Sudah Punya Akun? Login di sini</a>
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            <a href="{{url('lostpassword')}}" class="btn btn-outline-secondary pd-x-25 tx-right mg-b-20">Lost Password?</a>
          </p>
          <p style="text-align:justify!important;"> <small>FinanciaFX memberikan kenyamanan terbaik dalam Investasi Forex karena anda sebagai Nasabah akan diberikan spread terendah yang mempermudah dalam pencapaian profit. Dalam melakukan Trading Forex dan Emas online di FinanciaFX Nasabah akan mendapatkan kemudahan dalam melakukan investasi. Bagi Anda yang trading Forex dan Emas di FinanciaFX sangatlah mudah, karena hanya cukup register lalu buka akun, Deposit minimal Rp 500.000,- / $50. Bank lokal yang kami pergunakan adalah Bank BCA, Mandiri, BRI dan BNI. Kami menyediakan layanan Live Chat support pada jam kerja (08.30-17.30 WIB) dari Senin-Jumat.</small> </p>

          <p class="tx-12">&copy; Copyright {{date('Y')}}. All Rights Reserved.</p>
          <small class="text-justify" id="disclaim">Disclaimer : Dalam trading forex tidak ada jaminan bahwa Anda akan untung, dan Forex Trading merupakan kegiatan usaha berisiko "high gain high risk". Di dalam trading forex, Anda dapat menderita kerugian keseluruhan atas investasi Anda.</small>

        </div><!-- signin-box -->
      </div><!-- signin-left -->
    </div><!-- d-flex -->


    <script src="{{url('/')}}/lib/jquery/js/jquery.js"></script>
    <script src="{{url('/')}}/lib/popper.js/js/popper.js"></script>
    <script src="{{url('/')}}/lib/bootstrap/js/bootstrap.js"></script>
    {!! NoCaptcha::renderJs() !!}
    <script src="{{url('/')}}/js/slim.js"></script>
    </script>
  </body>
  </html>
