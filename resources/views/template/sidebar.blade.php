<div class="slim-sidebar">
  <label class="sidebar-label">Navigation</label>

  <ul class="nav nav-sidebar">
    <li class="sidebar-nav-item">
      <a href="{{route('dashboard')}}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="home-outline"></ion-icon> Dashboard</a>
    </li>
    <li class="sidebar-nav-item with-sub">
      <a href="javascript:void(0)" class="sidebar-nav-link"><ion-icon class="mr-3" style="color: #1b84e7" name="reorder-four-outline"></ion-icon> Demo Menu</a>
        <ul class="nav sidebar-nav-sub">
          <li class="nav-sub-item"><a href="{{route('demoKlasemen')}}" class="nav-sub-link move__page">Klasemen</a></li>
          <li class="nav-sub-item"><a href="{{route('materiPage')}}" class="nav-sub-link move__page">Materi</a></li>
          <li class="nav-sub-item"><a href="{{route('zoomPage')}}" class="nav-sub-link move__page">Zoom Meeting</a></li>
          <li class="nav-sub-item"><a href="{{route('telegramPage')}}" class="nav-sub-link move__page">Telegram Group</a></li>
          <li class="nav-sub-item"><a href="index5.html" class="nav-sub-link move__page">Demo Account</a></li>
        </ul>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{route('pendaftaranGrandPrix')}}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="speedometer-outline"></ion-icon> Pendaftaran GrandPrix</a>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{route('listAccount')}}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" name="person-outline" style="color: #1b84e7"></ion-icon> Akun Trading</a>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{route('listDeposit')}}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="wallet-outline"></ion-icon> Deposit</a>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{route('listWithdraw')}}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="folder-open-outline"></ion-icon> Withdraw</a>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{ route('klasemenPage') }}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="reorder-four-outline"></ion-icon> Klasemen</a>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{ route('announcementPage') }}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="megaphone-outline"></ion-icon> Pengumuman</a>
    </li>
    <li class="sidebar-nav-item">
      <a href="{{route('helpPage')}}" class="sidebar-nav-link move__page"><ion-icon class="mr-3" style="color: #1b84e7" name="chatbubbles-outline"></ion-icon> Contact Us</a>
    </li>
    
    
    
    
    
  </ul>
</div><!-- slim-sidebar -->
