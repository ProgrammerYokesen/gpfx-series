<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf_token" content="{{ csrf_token() }}">
    <!-- Meta -->
    <meta name="description" content="GrandPrix Forex">
    <meta name="author" content="FinanciaFx">

    <title>Grand Prix Forex</title>
    {{-- <meta property="og:url"                content="https://ims.financiafx.id/lp/robot/newbie" />
  <meta property="og:type"               content="landing page" />
  <meta property="og:title"              content="Gajahmada Robot Trading Forex | FinanciaFx" />
  <meta property="og:description"        content="Robot Trading Forex Tanpa Perlu Belajar | FinanciaFx" />
  <meta property="og:image"              content="{{url('/')}}/ea/banner/instagram-financiafx-alt-8-16x9.jpg" /> --}}
    <!-- Vendor css -->
    <link href="{{ url('/') }}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{ url('/') }}/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{ url('/') }}/css/slim.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/sweetalert.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/dashboard.css">

    <style media="screen">
        .gradient_right {
            color: #ffffff;
            background-color: #ff0000;
            /* For browsers that do not support gradients */
            background-image: linear-gradient(to top right, #0a2e7d, #061541);
            /* Standard syntax (must be last) */
        }

        .gradient_left {
            background-color: #c49229;
            /* For browsers that do not support gradients */
            background-image: linear-gradient(to top right, #c49229, #f9e655, #91660f);
            /* Standard syntax (must be last) */
            color: black;
        }

    </style>

</head>

<body>

    <div class="d-md-flex">
        <div class="signin-left">
            <div class="text-center">
                <h1>LOGIN SEKARANG</h1>
                <h2>Untuk Mengikuti</h2>
                <img src="{{ asset('images/logo.png') }}" alt="">
            </div>
        </div><!-- signin-right -->
        <div class="signin-right">
            <div class="signin-box signup" style="background-color:rgba(0, 0, 0, 0.0);">
                <a class="slim-logo center" href="https://ims.financiafx.id" target="_blank">
                    <h2 style="margin-left: 38%"><img src="/img/logo.png" alt="" width="120"></h2>
                </a>

                <form class="" action="{{ route('postLogins') }}" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Enter your email" required>
                    </div><!-- form-group -->
                    <div class="form-group mg-b-50">
                        <input type="password" class="form-control" name="password" placeholder="Enter your password"
                            required>
                    </div><!-- form-group -->
                    @csrf

                    <button class="btn btn-primary btn-block btn-signin">Login</button>
                </form>

                <p>
                    <a href="{{ route('daftar') }}" class="btn btn-secondary pd-x-25 tx-right mg-b-20">Don't have
                        account?</a>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <a href="{{ route('forgotPass') }}" class="btn btn-outline-primary pd-x-25 tx-right mg-b-20">Forgot
                        Password?</a>
                </p>


                <p class="tx-12">&copy; Copyright {{ date('Y') }}. All Rights Reserved.</p>
            </div><!-- signin-box -->
        </div><!-- signin-left -->
    </div><!-- d-flex -->


    <script src="{{ url('/') }}/lib/jquery/js/jquery.js"></script>
    <script src="{{ url('/') }}/lib/popper.js/js/popper.js"></script>
    <script src="{{ url('/') }}/lib/bootstrap/js/bootstrap.js"></script>
    {!! NoCaptcha::renderJs() !!}
    <script src="{{ url('/') }}/js/slim.js"></script>
    </script>
</body>

</html>
