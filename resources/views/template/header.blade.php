
<div class="slim-header with-sidebar">
  <div class="container-fluid">
    <div class="slim-header-left">
      <!--<h2 class="slim-logo"><a href="{{route('dashboard')}}">{{ config('app.name', 'GrandPrix Forex') }}</a></h2>-->
      <h2 class="slim-logo"><a href="{{route('dashboard')}}"><img src="{{asset('./images/logo-dashboard.svg')}}" style="width:100px; height: 50px" /></a></h2>
      <a href="" id="slimSidebarMenu" class="slim-sidebar-menu"><span></span></a>
      
    </div><!-- slim-header-left -->
    
    <div class="slim-header-right">
      <div class="dropdown dropdown-c">
        <a href="#" class="logged-user" data-toggle="dropdown">
          <img src="{{url('/')}}/img/user-default.jpg" alt="{{Session::get('user.name')}}">
          <span>{{Session::get('user.name')}}</span>
          <i class="fa fa-angle-down"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <nav class="nav">
            <a href="javascript:void(0)" onclick="swal({
                    title: 'Yakin, Kamu mau logout?',
                    type:'info',
                    showCancelButton:true,
                    allowOutsideClick:true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Logout',
                    cancelButtonText: 'Cancel',
                    closeOnConfirm: false
                    }, function(){
                    location.href = '{{ route("logout") }}';

                    });" class="nav-link"><i class="icon ion-forward"></i> Sign Out</a>
            <a href="" class="nav-link"><i class="icon ion-people"></i> Ganti Password</a>
          </nav>
        </div><!-- dropdown-menu -->
      </div><!-- dropdown -->
    </div><!-- header-right -->
  </div><!-- container-fluid -->
</div><!-- slim-header -->
