@extends("crudbooster::admin_template")
@section('css')
     <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" integrity="sha512-bYPO5jmStZ9WI2602V2zaivdAnbAhtfzmxnEGh9RwtlI00I9s8ulGe4oBa5XxiC6tCITJH/QG70jswBhbLkxPw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
@section('js')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js" integrity="sha512-AIOTidJAcHBH2G/oZv9viEGXRqDNmfdPVPYOYKGy3fti0xIplnlgMHUGfuNRzC6FkzIo0iIxgFnr9RikFxK+sw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
<script type="text/javascript">
    $(function () {
        $('#sendat').datetimepicker({
            format : "Y-m-d H:i:s"
        });
    });
 </script>
@endsection
@section('content')
<form style="background: white; padding:10px; border-radius:10px;" method="POST" action="{{ route('processEmailQueue') }}">
    @csrf
    <div class="form-group">
        <label for="exampleFormControlSelect1">Template</label>
        <select class="form-control" id="exampleFormControlSelect1" name="email_template_id">
            @foreach($templates as $template)
                <option value="{{ $template->id }}">{{ $template->name }}</option>
            @endforeach
        </select>
      </div>
    <div class="form-group">
        <label for="emailList">Email</label>
        <select class="js-example-basic-multiple form-control" name="custom_field[]" multiple="multiple" id="emailList">
            <option value="all">Pick all</option>
            @foreach($datas as $data)
                <option value="{{ $data->email }}">{{ $data->email }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="sendat">Send At</label>
        <input class="form-control" type="datetime" name="send_at" id="sendat">
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Type</label>
        <select class="form-control" id="exampleFormControlSelect1" name="type">
          <option value="onetime">onetime</option>
          <option value="weekly">weekly</option>
        </select>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection