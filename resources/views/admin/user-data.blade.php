@extends("crudbooster::admin_template")


@section("content")
<div class="row" style="margin-bottom:10px;">
    <div class="col-md-4">
        <a class="btn btn-primary" href="{{route('membersExport')}}" style="padding:5px;">Export to excell</a>

        <a class="btn btn-warning" href="{{route('refreshMember')}}" style="padding:5px;">Refresh Member</a>
    </div>
</div>
<table class="table" style="background-color:white;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">uuid</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Campaign</th>
      <th scope="col">Status</th>
      <th scope="col">Detail</th>
    </tr>
  </thead>
  <tbody>
     @php $i = 1; @endphp
     @foreach($result as $res)
    <tr>
      <th scope="row">{{$i}}</th>
      <th scope="row">{{$res->uuid}}</th>
      <td>{{$res->name}}</td>
      <td>{{$res->email}}</td>
      <td>{{$res->campaign}}</td>
      <td>{{$res->status}}</td>
      <td><a href={{route('userDetail', $res->uuid)}} class="btn btn-alert"><i class="fa fa-eye"></i></a></td>
    </tr>
    @php $i++; @endphp
    @endforeach
  </tbody>
</table>
@endsection
