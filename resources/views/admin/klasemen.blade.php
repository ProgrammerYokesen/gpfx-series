@extends("crudbooster::admin_template")


@section("content")
<table class="table" style="background-color:white;border-radius:10px;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Profit</th>
    </tr>
  </thead>
  <tbody>
     @php $i = 1; @endphp
     @forelse($datas as $res)
    <tr>
      <th scope="row">{{$i}}</th>
      <td>{{$res->name}}</td>
      <td>{{$res->email}}</td>
      <td>{{$res->profit}}</td>
    </tr>
    @php $i++; @endphp
    @empty
    <tr>
    <td colspan="4" class="text-center">Data kosong</td>
    </tr>
    @endforelse
  </tbody>
</table>
@endsection
