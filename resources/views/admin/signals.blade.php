@extends("crudbooster::admin_template")


@section("content")
<table class="table" style="background-color:white;border-radius:10px;">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">Campaign</th>
      <th scope="col">Status</th>
      <th scope="col">Detail</th>
    </tr>
  </thead>
  <tbody>
     @php $i = 1; @endphp
     @foreach($result as $res)
    <tr>
      <th scope="row">{{$i}}</th>
      <th scope="row">{{$res->id}}</th>
      <td>{{$res->name}}</td>
      <td>{{$res->email}}</td>
      <td>{{$res->campaign}}</td>
      <td>{{$res->status}}</td>
      <td><a href={{route('userDetail', $res->id)}} class="btn btn-alert"><i class="fa fa-eye"></i></a></td>
    </tr>
    @php $i++; @endphp
    @endforeach
  </tbody>
</table>
@endsection
