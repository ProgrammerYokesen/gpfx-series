<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::middleware(['visitordata'])->group(function () {
//   Route::get('/', 'UserController@test');
  Route::get('/', 'PageController@homePage')->name('homePage');
  Route::get('/daftar', 'RegisterController@register')->name('daftar');
  Route::post('/go/daftar', 'RegisterController@processRegister')->name('postDaftar');

  Route::get('/masuk', 'LoginController@login')->name('masuk');
  Route::post('/go/masuk', 'LoginController@processLogin')->name('postLogins');
  Route::get('/keluar', 'LoginController@logout')->name('logout');

  Route::post('/send-otp','whatsappOtp@registerUser')->name('whatsapOtp');
  Route::get('/verify-page', 'whatsappOtp@registerUser')->name('verifyPage');
  Route::post('/send-data-register','whatsappOtp@sendDataRegister')->name('postVerify');
  Route::get('/klasemen', 'PageController@leaderboardPage')->name('leaderboardPage');
  Route::get('/klasemen-total', 'PageController@leaderboardGpPage')->name('leaderboardGpPage');
  Route::get('/forgot-password', 'PageController@forgotPass')->name('forgotPass');
  Route::post('/forgot-password', 'PageController@PostforgotPass')->name('postForgotPass');
  Route::get('/reset-password', 'PageController@resetPass')->name('resetPass');
  Route::post('/reset-password', 'PageController@postResetPass')->name('postResetPass');
  Route::get('/reset-success', 'PageController@resetSuccess')->name('resetSuccess');

});

Route::middleware(['logon'])->group(function () {
  Route::get('/dashboard','DashboardController@profile')->name('dashboard');
  Route::get('/grandprix/pendaftaran','GrandprixController@pendaftaran')->name('pendaftaranGrandPrix');
  Route::post('/grandprix/action/daftar','GrandprixController@update')->name('postDaftarGrandPrix');

  Route::get('/grandprix/account/request','AccountController@requestAccount')->name('requestAccount');
  Route::get('/grandprix/account/list','AccountController@listAccount')->name('listAccount');
  Route::get('/grandprix/account/reset-password','AccountController@resetAccount')->name('accountReset');

  Route::get('/grandprix/deposit/list','DepositController@depositList')->name('listDeposit');
  Route::post('/grandprix/deposit/request','DepositController@depositRequest')->name('depositCreate');

  Route::get('/grandprix/withdraw/list','WithdrawController@withdrawList')->name('listWithdraw');
  Route::post('/grandprix/withdraw/request','WithdrawController@withdrawRequest')->name('withdrawCreate');
  
  Route::get('/grandprix/contact-us','PageController@helpPage')->name('helpPage');
  Route::get('grandprix/klasemen','klasemenController@klasemenPage')->name('klasemenPage');
  Route::get('grandprix/announcement','klasemenController@announcementPage')->name('announcementPage');
  Route::get('grandprix/demo-klasemen','PageController@demoKlasemen')->name('demoKlasemen');
  Route::get('grandprix/materi','PageController@materiPage')->name('materiPage');
  Route::get('grandprix/telegram','PageController@telegramPage')->name('telegramPage');
  Route::get('grandprix/zoom','PageController@zoomPage')->name('zoomPage');
});

// Route Lab
Route::prefix('lab')->group(function () {
    Route::get('/gpfxseries_register','labController@registerData');
    Route::get('/verify','labController@verify');
    Route::get('/trading-account','labController@tradingAccount');
    Route::get('/meta-trader','labController@metaTraderData');
    // Route::get('/announcement','labController@announcementPage')->name('announcementPage');
    // Route::get('/klasemen','labController@klasemenPage')->name('klasemenPage');
});

//Route from andika
// Route admin
Route::group(['prefix' => 'competitionadmin',  'middleware' => 'adminLogin'], function () {
    Route::get('/members','adminController@userdata');
    Route::get('/refresh-members','adminController@refreshMember')->name('refreshMember');
    Route::get('/signals','adminController@signals');
    Route::get('/klasemen','adminController@klasemen');
    Route::get('/members/{id}','adminController@userdataDetail')->name('userDetail');
    Route::get('/members-export','adminController@exportExcel')->name('membersExport');
    Route::post('/post-activity','adminController@processActivity')->name('processActivity');
    Route::post('/process-status','adminController@changeUserStatus')->name('processStatus');
    Route::post('/process-follow-up','adminController@followUp')->name('processFollowUp');
    Route::get('/email_queues/add-data', 'adminController@emailQueue')->name('addEmailQueue');
    Route::post('/email_queues/add-data', 'adminController@processEmailQueue')->name('processEmailQueue');
});
