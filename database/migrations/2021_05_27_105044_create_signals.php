<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticket',30);
            $table->string('login',30);
            $table->string('openTime');
            $table->string('type', 10);
            $table->string('item', 50);
            $table->string('size');
            $table->string('openPrice');
            $table->string('stopLoss');
            $table->string('takeProfit');
            $table->string('closeTime');
            $table->string('closePrice');
            $table->string('profit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signals');
    }
}
