<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUtmCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registerData', function (Blueprint $table) {
            $table->string('ref')->nullable();
            $table->text('campaign')->nullable();
            $table->text('so')->nullable();
            $table->string('uuid')->nullable();
            $table->string('disclaimer')->nullable();
            $table->string('dari')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registerData', function (Blueprint $table) {
            //
        });
    }
}
